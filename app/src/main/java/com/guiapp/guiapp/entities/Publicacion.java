package com.guiapp.guiapp.entities;

import com.guiapp.guiapp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajade on 14/12/2017.
 */

public class Publicacion {
    public static final String TABLE_NAME = "publicaciones";
    public static final String CREATE_TABLE = "CREATE TABLE [publicaciones](" +
            "[id] VARCHAR(36) UNIQUE, [idempresa] VARCHAR(36), [titulo] VARCHAR2, " +
            "[imagen] VERCHAR2, [descripcion] VERCHAR2, [destacada] INT, " +
            "[fecmodificacion] VARCHAR, [dirty] INT);";

    private String id;
    private String idempresa;
    private String razonsocial;
    private String titulo;
    private String imagen;
    private String descripcion;
    private int destacada;
    private String fecmodificacion;
    private int dirty;
    private String idcuenta;

    public Publicacion() {
        id = "";
        idempresa = "";
        dirty = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(String idempresa) {
        this.idempresa = idempresa;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getDestacada() {
        return destacada;
    }

    public void setDestacada(int destacada) {
        this.destacada = destacada;
    }

    public String getFecmodificacion() {
        return fecmodificacion;
    }

    public void setFecmodificacion(String fecmodificacion) {
        this.fecmodificacion = fecmodificacion;
    }

    public int getDirty() {
        return dirty;
    }

    public void setDirty(int dirty) {
        this.dirty = dirty;
    }

    public String getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta) {
        this.idcuenta = idcuenta;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("idempresa", idempresa);
        map.put("razonsocial", razonsocial);
        map.put("titulo", titulo);
        map.put("imagen", imagen);
        map.put("descripcion", descripcion);
        map.put("destacada", destacada + "");
        map.put("fecmodificacion", fecmodificacion);
        map.put("dirty", dirty + "");
        return map;
    }
}
