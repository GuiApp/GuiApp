package com.guiapp.guiapp.entities;

/**
 * Created by ajade on 03/07/2018.
 */

public class EventoTipo {

    private long id;
    private String tipo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
