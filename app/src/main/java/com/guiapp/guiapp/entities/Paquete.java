package com.guiapp.guiapp.entities;

import java.util.ArrayList;

/**
 * Created by ajade on 14/12/2017.
 */

public class Paquete {
    public static final String TABLE_NAME = "paquetes";
    /*
    public static final String CREATE_TABLE = "CREATE TABLE [paquetes](" +
            "[id] BIGINT UNIQUE, [idempresa] BIGINT, [idimagen] BIGINT, [titulo] VARCHAR2, " +
            "[imagen] VERCHAR2, [descripcion] VERCHAR2, [razonsocial] VERCHAR2, [destacada] INT, " +
            "[fecmodificacion] VARCHAR, [dirty] INT);";
    */

    private int id;
    private String imagen;
    private String nombre;
    private String descripcion;
    private double costo;
    private int duracion;
    private String logo;
    private int activo;
    private String plazo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }
}
