package com.guiapp.guiapp.entities;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajade on 15/12/2017.
 */

public class Empresa {
    public static final String TABLE_NAME = "empresas";

    private String id;
    private String razonsocial;
    private long cuit;
    private String mediodepago;
    private String estado;
    private String logo;
    private String facebook;
    private String twitter;
    private String instagram;
    private String palabrasclave;
    private int dirty;
    private String web;
    private String descripcion;
    private String idcuenta;

    public Empresa() {
        this.id = "";
        this.razonsocial = "";
        this.cuit = 0;
        this.mediodepago = "";
        this.estado = "";
        this.logo = "";
        this.facebook = "";
        this.twitter = "";
        this.instagram = "";
        this.palabrasclave = "";
        this.dirty = 1;
        this.web = "";
        this.descripcion = "";
        this.idcuenta = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public long getCuit() {
        return cuit;
    }

    public void setCuit(long cuit) {
        this.cuit = cuit;
    }

    public String getMediodepago() {
        return mediodepago;
    }

    public void setMediodepago(String mediodepago) {
        this.mediodepago = mediodepago;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getPalabrasclave() {
        return palabrasclave;
    }

    public void setPalabrasclave(String palabrasclave) {
        this.palabrasclave = palabrasclave;
    }

    public int getDirty() {
        return dirty;
    }

    public void setDirty(int dirty) {
        this.dirty = dirty;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta) {
        this.idcuenta = idcuenta;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("razonsocial", this.razonsocial);
        map.put("cuit", this.cuit + "");
        map.put("mediodepago", this.mediodepago);
        map.put("estado", this.estado);
        map.put("facebook", this.facebook);
        map.put("twitter", this.twitter);
        map.put("instagram", this.instagram);
        map.put("palabrasclave", this.palabrasclave);
        map.put("dirty", dirty + "");
        map.put("web", this.web);
        map.put("descripcion", this.descripcion);
        map.put("idcuenta", this.idcuenta);
        map.put("logo", this.logo);
        return map;
    }
}
