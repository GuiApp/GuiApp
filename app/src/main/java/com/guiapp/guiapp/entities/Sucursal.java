package com.guiapp.guiapp.entities;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajade on 15/12/2017.
 */

public class Sucursal {
    public static final String TABLE_NAME = "eventos";

    private String id;
    private String idempresa;
    private String direccion;
    private int idpais;
    private int idprovincia;
    private int idlocalidad;
    private String telefono;
    private int delivery;
    private int veinticuatrohs;
    private String diashorarios;
    private int dirty;
    private String palabrasclave;
    private double latitud;
    private double longitud;
    private String estado;
    private String razonsocial;

    public Sucursal() {
        this.id = "";
        this.dirty = 1;
        this.delivery = 0;
        this.veinticuatrohs = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(String idempresa) {
        this.idempresa = idempresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getIdpais() {
        return idpais;
    }

    public void setIdpais(int idpais) {
        this.idpais = idpais;
    }

    public int getIdprovincia() {
        return idprovincia;
    }

    public void setIdprovincia(int idprovincia) {
        this.idprovincia = idprovincia;
    }

    public int getIdlocalidad() {
        return idlocalidad;
    }

    public void setIdlocalidad(int idlocalidad) {
        this.idlocalidad = idlocalidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getVeinticuatrohs() {
        return veinticuatrohs;
    }

    public void setVeinticuatrohs(int veinticuatrohs) {
        this.veinticuatrohs = veinticuatrohs;
    }

    public String getDiashorarios() {
        return diashorarios;
    }

    public void setDiashorarios(String diashorarios) {
        this.diashorarios = diashorarios;
    }

    public int getDirty() {
        return dirty;
    }

    public void setDirty(int dirty) {
        this.dirty = dirty;
    }

    public String getPalabrasclave() {
        return palabrasclave;
    }

    public void setPalabrasclave(String palabrasclave) {
        this.palabrasclave = palabrasclave;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public static ArrayList<Sucursal> getSucursalesByIdEmpresa(String idempresa) {
        //TODO: este código es para test inicial
        ArrayList<Sucursal> array = new ArrayList<Sucursal>();
        Sucursal item = new Sucursal();
        item.setId("1");
        item.setDireccion("Av. Maipú");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("olivar");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("Pellegrini");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("Santa Ana");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("Poncho Verde");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("Sarmiento");
        array.add(item);

        item = new Sucursal();
        item.setId("1");
        item.setDireccion("El Maestro");
        array.add(item);

        return array;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("idempresa", this.idempresa);
        map.put("direccion", this.direccion);
        map.put("telefono", this.telefono);
        map.put("delivery", this.delivery + "");
        map.put("veinticuatrohs", this.veinticuatrohs + "");
        map.put("diashorarios", this.diashorarios);
        map.put("dirty", this.dirty + "");
        map.put("palabrasclave", this.palabrasclave);
        map.put("latitud", this.latitud + "");
        map.put("longitud", this.longitud+ "");
        return map;
    }
}
