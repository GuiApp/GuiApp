package com.guiapp.guiapp.entities;

import java.util.HashMap;

/**
 * Created by ajade on 20/05/2018.
 */

public class CategoriaXEmpresa {
    private int id;
    private int idcategoria;
    private String idempresa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(String idempresa) {
        this.idempresa = idempresa;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id + "");
        map.put("idcategoria", this.idcategoria + "");
        map.put("idempresa", this.idempresa);

        return map;
    }
}
