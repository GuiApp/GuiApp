package com.guiapp.guiapp.entities;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.enums.Modulos;

import java.util.Iterator;

/**
 * Created by ajade on 14/12/2017.
 *
 */

public class Funcion {
    /*
    public static final String TABLE_NAME = "funciones";
    public static final String CREATE_TABLE = "CREATE TABLE [funciones](" +
            "[id] BIGINT UNIQUE, [descripcion] VARCHAR2, " +
            "[cantidad] INT, [modulo] INT, [dirty] INT);";
    */

    private int id;
    private String descripcion;
    private int cantidad;
    private int idmodulo;
    private String modulo;
    private int limalcanzado;

    private JsonArray mJson;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdmodulo() {
        return idmodulo;
    }

    public void setIdmodulo(int idmodulo) {
        this.idmodulo = idmodulo;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public int getLimalcanzado() {
        return limalcanzado;
    }

    public void setLimalcanzado(int limalcanzado) {
        this.limalcanzado = limalcanzado;
    }

    public void loadPermisos(Context ctx) {
        String jsonString = SessionProcessor.getIdFunciones(ctx);
        Gson gson = new Gson();
        JsonParser jsonParser = new JsonParser();
        mJson = (JsonArray) jsonParser.parse(jsonString);
    }

    public boolean getLimAlcanzado(Modulos mod) {
        Iterator itr = this.mJson.iterator();
        Gson gson = new Gson();
        while(itr.hasNext()) {
            Funcion fun = gson.fromJson((JsonElement) itr.next(), Funcion.class);
            if(fun.getIdmodulo() == mod.ordinal()) {
                if(fun.getLimalcanzado() == 1) return true;
            }
        }

        return false;
    }
}
