package com.guiapp.guiapp.entities;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by usuario on 10/10/2017.
 */

public abstract class EntityAbstract
{
    public String save(SQLiteDatabase db, String tableName, ContentValues contentValues, String id)
    {
        String mess = "";

        try
        {
            int idRet = (int) db.insertWithOnConflict(tableName, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (idRet == -1)
            {
                db.update(tableName, contentValues, "id=?", new String[] {id + ""});
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }

        return mess;
    }
}
