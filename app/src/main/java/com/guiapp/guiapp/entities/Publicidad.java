package com.guiapp.guiapp.entities;

/**
 * Created by ajade on 08/02/2018.
 */

public class Publicidad {
    private long id;
    private long idsponsor;
    private String fecinicio;
    private String fecfin;
    private String imagen;
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdsponsor() {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor) {
        this.idsponsor = idsponsor;
    }

    public String getFecinicio() {
        return fecinicio;
    }

    public void setFecinicio(String fecinicio) {
        this.fecinicio = fecinicio;
    }

    public String getFecfin() {
        return fecfin;
    }

    public void setFecfin(String fecfin) {
        this.fecfin = fecfin;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
