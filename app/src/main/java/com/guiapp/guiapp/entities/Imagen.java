package com.guiapp.guiapp.entities;

/**
 * Created by ajade on 15/12/2017.
 */

public class Imagen {
    public static final String TABLE_NAME = "imagen";

    private String id;
    private String idempresa;
    private String url;

    public Imagen()  {
        this.id = "";
        this.idempresa = "";
        this.url = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(String idempresa) {
        this.idempresa = idempresa;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
