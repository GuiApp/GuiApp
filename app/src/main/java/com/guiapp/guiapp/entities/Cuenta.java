package com.guiapp.guiapp.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;

/**
 * Created by ajade on 02/02/2018.
 */

public class Cuenta  extends EntityAbstract{
    public static final String INVITADO = "INVITADO";

    public static final String TABLE_NAME = "cuentas";
    public static final String CREATE_TABLE =
            "CREATE TABLE [cuentas]([" +
                    "id] VARCHAR2 PRIMARY KEY UNIQUE, [nombre] VARCHAR2, [email] VARCHAR2, " +
                    "[provider] VARCHAR2, [notificaciones] INTEGER, [sync] INTEGER);";

    private String id;
    private String nombre;
    private String email;
    private String provider;
    private int notificaciones;
    private int sync;

    public Cuenta() {
        this.id = "";
        this.nombre = INVITADO;
        this.email = "";
        this.provider = "FACEBOOK";
        this.notificaciones = 0;
        this.sync = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(int notificaciones) {
        this.notificaciones = notificaciones;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.id);
        contentValues.put("nombre", this.nombre);
        contentValues.put("email", this.email);
        contentValues.put("provider", this.provider);
        contentValues.put("notificaciones", this.notificaciones + "");
        contentValues.put("sync", this.sync + "");

        return super.save(db, TABLE_NAME, contentValues, this.id);

//        String mess = "";
//
//        try {
//            int id = (int) db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//            if (id == -1) {
//                db.update(TABLE_NAME, contentValues, "id='?'", new String[] {this.id});
//            }
//        } catch (Exception e) {
//            mess = e.getMessage();
//        }
//
//        return mess;
    }

    public String fill(SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM cuentas LIMIT 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setNombre(cur.getString(cur.getColumnIndex("nombre")));
                this.setEmail(cur.getString(cur.getColumnIndex("email")));
                this.setProvider(cur.getString(cur.getColumnIndex("provider")));
                this.setNotificaciones(cur.getInt(cur.getColumnIndex("notificaciones")));
                this.setSync(cur.getInt(cur.getColumnIndex("sync")));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("nombre", this.nombre);
        map.put("email", this.email);
        map.put("provider", this.provider);
        map.put("notificaciones", this.notificaciones + "");

        return map;
    }
}
