package com.guiapp.guiapp.entities;

import com.guiapp.guiapp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajade on 14/12/2017.
 */

public class Evento {
    public static final String TABLE_NAME = "eventos";
    /*
    public static final String CREATE_TABLE = "CREATE TABLE [eventos](" +
            "[id] BIGINT UNIQUE, [idempresa] BIGINT, [idimagen] BIGINT, [titulo] VARCHAR2, " +
            "[imagen] VERCHAR2, [descripcion] VERCHAR2, [razonsocial] VERCHAR2, [destacada] INT, " +
            "[fecmodificacion] VARCHAR, [dirty] INT);";
    */

    private String id;
    private String nombre;
    private String descripcion;
    private String imagen;
    private long idtipo;
    private String tipo;
    private String ubicacion;
    private String fecinicio;
    private String fecfin;
    private String idempresa;
    private String razonsocial;
    private String horainicio;
    private String horafin;
    private String idcuenta;

    public Evento() {
        this.id = "";
        this.fecinicio = "";
        this.fecfin = "";
        this.horainicio = "";
        this.horafin = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public long getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(long idtipo) {
        this.idtipo = idtipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFecinicio() {
        return fecinicio;
    }

    public void setFecinicio(String fecinicio) {
        this.fecinicio = fecinicio;
    }

    public String getFecfin() {
        return fecfin;
    }

    public void setFecfin(String fecfin) {
        this.fecfin = fecfin;
    }

    public String getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(String idempresa) {
        this.idempresa = idempresa;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getHorainicio() {
        return horainicio;
    }

    public void setHorainicio(String horainicio) {
        this.horainicio = horainicio;
    }

    public String getHorafin() {
        return horafin;
    }

    public void setHorafin(String horafin) {
        this.horafin = horafin;
    }

    public String getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta) {
        this.idcuenta = idcuenta;
    }

    public String getFormatedFecinicio() {
        String[] fecha = this.fecinicio.split("-");
        return fecha[2] + "-" + fecha[1] + "-" + fecha[0];
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("descripcion", this.descripcion);
        map.put("nombre", this.nombre);
        map.put("imagen", this.imagen);
        map.put("idtipo", this.idtipo + "");
        map.put("ubicacion", this.ubicacion);
        map.put("fecinicio", this.fecinicio);
        map.put("fecfin", this.fecfin);
        map.put("idempresa", this.idempresa);
        map.put("horainicio", this.horainicio);
        map.put("horafin", horafin);
        map.put("idcuenta", this.idcuenta);
        return map;
    }
}
