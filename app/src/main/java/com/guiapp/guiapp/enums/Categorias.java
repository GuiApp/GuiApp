package com.guiapp.guiapp.enums;

import java.util.ArrayList;

/**
 * Created by ajade on 08/02/2018.
 */

public enum Categorias {
    moda,
    gastronomia,
    salud,
    entretenimiento,
    transporte,
    servicios,
    turismo,
    calendario,
    mas;

    public String getString(){
        switch (this) {
            case moda:
                return "Moda";
            case gastronomia:
                return "Gastronomia";
            case salud:
                return "Salud";
            case entretenimiento:
                return "Entretemiento";
            case transporte:
                return "Trasporte";
            case servicios:
                return "Servicios";
            case turismo:
                return "Turismo";
            case calendario:
                return "Calendario";
            default:
                return "Mas";
        }
    }

    public static ArrayList<Categorias> getArray() {
        ArrayList<Categorias> arr = new ArrayList<Categorias>();
        arr.add(Categorias.moda);
        arr.add(Categorias.gastronomia);
        arr.add(Categorias.salud);
        arr.add(Categorias.entretenimiento);
        arr.add(Categorias.transporte);
        arr.add(Categorias.servicios);
        arr.add(Categorias.turismo);
        arr.add(Categorias.calendario);
        arr.add(Categorias.mas);
        return arr;
    }
}