package com.guiapp.guiapp.enums;

/**
 * Created by ajade on 08/02/2018.
 */
public enum Modulos {
    unknown,
    publicacion,
    empresa,
    sucursal,
    eventos;

    public String getString(){
        switch (this) {
            case publicacion:
                return "Publicación";
            case empresa:
                return "Empresa";
            case sucursal:
                return "Sucursal";
            case eventos:
                return "Eventos";
            default:
                return "Desconocido";
        }
    }
}