package com.guiapp.guiapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.Publicacion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ajade on 14/12/2017.
 */

public class CarouselCalendarioAdapter extends RecyclerView.Adapter<CarouselCalendarioAdapter.ItemView> {
    private ArrayList<Evento> mDataSet;
    private RecyclerViewOnItemClickListener mOnItemClickListener;

    public CarouselCalendarioAdapter(ArrayList<Evento> dataSet,
                                     @NonNull RecyclerViewOnItemClickListener
                                             onItemClickListener) {
        mDataSet = dataSet;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public CarouselCalendarioAdapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_carousel_calendario, parent, false);
        ItemView iv = new ItemView(v);
        return iv;
    }

    @Override
    public void onBindViewHolder(CarouselCalendarioAdapter.ItemView holder, int position) {
        Evento item = mDataSet.get(position);
        holder.mTipoEvento.setText(item.getTipo());
        holder.mEvento.setText(item.getNombre());
        holder.mDescripcion.setText(item.getDescripcion());

        String sfecha = "";
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date newDate=spf.parse(item.getFecinicio());
            spf= new SimpleDateFormat("dd MMM");
            sfecha = spf.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mFecha.setText(sfecha);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTipoEvento;
        private TextView mEvento;
        private TextView mDescripcion;
        private TextView mFecha;

        public ItemView(View itemView) {
            super(itemView);
            mTipoEvento = itemView.findViewById(R.id.tvTipoEvento);
            mEvento = itemView.findViewById(R.id.tvEvento);
            mDescripcion = itemView.findViewById(R.id.tvDescripcion);
            mFecha = itemView.findViewById(R.id.tvFecha);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

    public interface  RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}
