package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.Sucursal;

import java.util.ArrayList;

/**
 * Created by ajade on 15/12/2017.
 */

public class SucursalesEmpresaAdapter extends ArrayAdapter<Sucursal> {
    private ArrayList<Sucursal> mItems;
    private Context mContext;

    public SucursalesEmpresaAdapter(Context context, int textViewResourceId, ArrayList<Sucursal> items) {
        super(context, textViewResourceId, items);
        this.mItems = items;
        this.mContext = context;
    }

    @Override
    public Sucursal getItem(int pos)
    {
        return (Sucursal) mItems.get(pos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if (v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_sucursal_empresa, null);
        }

        Sucursal s = (Sucursal) mItems.get(position);
        if (s != null)
        {
            TextView f1 = v.findViewById(R.id.tvSucursal);
            f1.setText("Sucursal " + s.getDireccion());
        }
        return v;
    }
}