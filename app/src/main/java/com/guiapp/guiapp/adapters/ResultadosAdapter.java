package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.Empresa;
import com.guiapp.guiapp.entities.Sucursal;

import java.util.ArrayList;

/**
 * Created by ajade on 15/12/2017.
 */

public class ResultadosAdapter extends ArrayAdapter<Sucursal> {
    private ArrayList<Sucursal> mItems;
    private Context mContext;

    public  ResultadosAdapter(Context context, int textViewResourceId, ArrayList<Sucursal> items) {
        super(context, textViewResourceId, items);
        this.mItems = items;
        this.mContext = context;
    }

    @Override
    public Sucursal getItem(int pos)
    {
        return (Sucursal) mItems.get(pos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if (v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_resultado, null);
        }

        Sucursal s = (Sucursal) mItems.get(position);
        if (s != null)
        {
            TextView f1 = v.findViewById(R.id.tvRazonSocial);
            f1.setText(s.getRazonsocial());

            TextView f2 = v.findViewById(R.id.tvEstado);
            f2.setText(s.getEstado());

            TextView f3 = v.findViewById(R.id.tvDireccion);
            f3.setText(s.getDireccion());

            ImageView b1 = v.findViewById(R.id.iv24hs);
            if(s.getVeinticuatrohs() == 1)
                b1.setVisibility(View.VISIBLE);
            else
                b1.setVisibility(View.INVISIBLE);

            ImageView b2 = v.findViewById(R.id.ivDelivery);
            if(s.getDelivery() == 1)
                b2.setVisibility(View.VISIBLE);
            else
                b2.setVisibility(View.INVISIBLE);
        }
        return v;
    }
}