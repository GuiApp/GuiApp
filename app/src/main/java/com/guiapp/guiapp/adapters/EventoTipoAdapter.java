package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.EventoTipo;
import com.guiapp.guiapp.entities.Sucursal;

import java.util.ArrayList;

/**
 * Created by ajade on 15/12/2017.
 */

public class EventoTipoAdapter extends ArrayAdapter<EventoTipo> {
    private ArrayList<EventoTipo> mItems;
    private Context mContext;

    public EventoTipoAdapter(Context context, int textViewResourceId, ArrayList<EventoTipo> items) {
        super(context, textViewResourceId, items);
        this.mItems = items;
        this.mContext = context;
    }

    @Override
    public EventoTipo getItem(int pos)
    {
        return mItems.get(pos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if (v == null)
        {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_tipoevento, null);
        }

        EventoTipo s = mItems.get(position);
        if (s != null)
        {
            TextView f1 = v.findViewById(R.id.tvTipoEvento);
            f1.setText(s.getTipo());
        }
        return v;
    }
}