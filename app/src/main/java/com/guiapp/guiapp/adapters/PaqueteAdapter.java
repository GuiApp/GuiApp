package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.guiapp.guiapp.EmpresasActivity;
import com.guiapp.guiapp.R;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.entities.Paquete;
import com.guiapp.guiapp.entities.Publicacion;
import com.guiapp.guiapp.entities.Sucursal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ajade on 15/12/2017.
 */

public class PaqueteAdapter extends RecyclerView.Adapter<PaqueteAdapter.ItemView> {
    private ArrayList<Paquete> mDataSet;
    private ImageLoader imageLoader = MainApp.getPermission().getImageLoader();
    private RecyclerViewOnItemClickListener mOnItemClickListener;

    public PaqueteAdapter(ArrayList<Paquete> dataSet,
                          @NonNull RecyclerViewOnItemClickListener
                                  onItemClickListener) {
        mDataSet = dataSet;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public PaqueteAdapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_paquete_premium, parent, false);
        ItemView iv = new ItemView(v);
        return iv;
    }

    @Override
    public void onBindViewHolder(PaqueteAdapter.ItemView holder, int position) {
        Paquete item = mDataSet.get(position);
        holder.mTitulo.setText(item.getNombre());
        holder.mImagen.setImageUrl(item.getLogo(), imageLoader);
        holder.mDescripcion.setText(item.getDescripcion());
        if(item.getPlazo().length() > 0)
            holder.mCompprado.setText(item.getPlazo());
        else
            holder.mCompprado.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitulo;
        private TextView mDescripcion;
        private TextView mCompprado;
        private NetworkImageView mImagen;

        public ItemView(View itemView) {
            super(itemView);

            if (imageLoader == null)
                imageLoader = MainApp.getPermission().getImageLoader();

            mImagen = itemView.findViewById(R.id.ivImagen);
            mTitulo = itemView.findViewById(R.id.tvTitulo);
            mCompprado = itemView.findViewById(R.id.tvComprado);
            mDescripcion = itemView.findViewById(R.id.tvDescripcion);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

    public interface RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}