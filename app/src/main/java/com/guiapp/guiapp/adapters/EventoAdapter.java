package com.guiapp.guiapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.guiapp.guiapp.R;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.Paquete;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ajade on 15/12/2017.
 */

public class EventoAdapter extends RecyclerView.Adapter<EventoAdapter.ItemView> {
    private ArrayList<Evento> mDataSet;
    private RecyclerViewOnItemClickListener mOnItemClickListener;

    public EventoAdapter(ArrayList<Evento> dataSet,
                         @NonNull RecyclerViewOnItemClickListener
                                  onItemClickListener) {
        mDataSet = dataSet;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public EventoAdapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_evento, parent, false);
        ItemView iv = new ItemView(v);
        return iv;
    }

    @Override
    public void onBindViewHolder(EventoAdapter.ItemView holder, int position) {
        Evento item = mDataSet.get(position);
        Date date = Utils.stringToDate(item.getFecinicio());
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", new Locale("es", "ES"));
        holder.mFecha.setText(sdf.format(date));
        holder.mTitulo.setText(item.getNombre());
        holder.mLugar.setText(item.getUbicacion());
        String[] hora =  item.getHorainicio().split(":");
        holder.mHorario.setText("Horario: " + hora[0] + ":" + hora[1]);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mFecha;
        private TextView mTitulo;
        private TextView mLugar;
        private TextView mHorario;

        public ItemView(View itemView) {
            super(itemView);

            mFecha = itemView.findViewById(R.id.tvFecha);
            mTitulo = itemView.findViewById(R.id.tvTituloEvento);
            mLugar = itemView.findViewById(R.id.tvLugar);
            mHorario = itemView.findViewById(R.id.tvHorario);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

    public interface RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}