package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.Empresa;

import java.util.List;

/**
 * Created by ajade on 22/03/2018.
 */

public class EmpresasSpinAdapter extends ArrayAdapter<Empresa> {
    private Context mContext;
    private List<Empresa> mItems;
    private boolean disableNew;

    public EmpresasSpinAdapter(Context context, int textViewResourceId, List<Empresa> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.mItems = items;
        this.disableNew = false;
    }

    public void setDisableNew(boolean disableNew) {
        this.disableNew = disableNew;
    }

    @Override
    public boolean isEnabled(int position){
        if(position == 0 && this.disableNew) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Empresa getItem(int position) {
        return mItems.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_item, null);
        }
        TextView label = v.findViewById(R.id.tvItemSel);
        label.setText(mItems.get(position).getRazonsocial());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_dropdown_item, null);
        }
        TextView label = v.findViewById(R.id.tvItem);
        label.setText(mItems.get(position).getRazonsocial());
        if(position == 0 && this.disableNew) {
            label.setTextColor(Color.GRAY);
            label.setTypeface(null, Typeface.ITALIC);
        } else {
            label.setTextColor(Color.WHITE);
            label.setTypeface(null, Typeface.NORMAL);
        }

        return label;
    }
}
