package com.guiapp.guiapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.guiapp.guiapp.R;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.entities.Imagen;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ajade on 15/12/2017.
 */

public class GaleriaAdapter extends android.support.v7.widget.RecyclerView.Adapter<GaleriaAdapter.ItemView>  {
    private ArrayList<Imagen> mDataSet;
    ImageLoader imageLoader = MainApp.getPermission().getImageLoader();
    private RecyclerViewOnItemClickListener mOnItemClickListener;

    public GaleriaAdapter(ArrayList<Imagen> arrDataSet,
                          @NonNull RecyclerViewOnItemClickListener onItemClickListener) {
        mDataSet = arrDataSet;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public GaleriaAdapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_galeria, parent, false);
        GaleriaAdapter.ItemView iv = new GaleriaAdapter.ItemView(v);
        return iv;
    }

    @Override
    public void onBindViewHolder(GaleriaAdapter.ItemView holder, int position) {
        Imagen pub = mDataSet.get(position);
//        holder.mImagen.setImageUrl(pub.getUrl(), imageLoader);
        Picasso.get().invalidate(pub.getUrl());
        Picasso.get().load(pub.getUrl()).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.default_evento).error(R.drawable.default_evento).into(holder.mImagen);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
//        private NetworkImageView mImagen;
        private ImageView mImagen;

        public ItemView(View itemView) {
            super(itemView);
            if (imageLoader == null)
                imageLoader = MainApp.getPermission().getImageLoader();

            mImagen = itemView.findViewById(R.id.ivImagen);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

    public interface  RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}
