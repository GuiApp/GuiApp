package com.guiapp.guiapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.guiapp.guiapp.R;
import com.guiapp.guiapp.entities.EventoTipo;
import com.guiapp.guiapp.enums.Categorias;

import java.util.List;

/**
 * Created by ajade on 22/03/2018.
 */

public class EventoTipoSpinAdapter extends ArrayAdapter<EventoTipo> {
    private Context mContext;
    private List<EventoTipo> mItems;

    public EventoTipoSpinAdapter(Context context, int textViewResourceId, List<EventoTipo> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public EventoTipo getItem(int position) {
        return mItems.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_item, null);
        }
        TextView label = v.findViewById(R.id.tvItemSel);
        label.setText(mItems.get(position).getTipo());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.spinner_dropdown_item, null);
        }
        TextView label = v.findViewById(R.id.tvItem);
        label.setText(mItems.get(position).getTipo());

        return label;
    }
}
