package com.guiapp.guiapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.guiapp.guiapp.R;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.entities.Publicacion;

import java.util.ArrayList;

/**
 * Created by ajade on 14/12/2017.
 */

public class CarouselAdapter extends android.support.v7.widget.RecyclerView.Adapter<CarouselAdapter.ItemView> {
    private ArrayList<Publicacion> mDataSet;
    private ImageLoader imageLoader = MainApp.getPermission().getImageLoader();
    private RecyclerViewOnItemClickListener mOnItemClickListener;
    private int mHeight;

    public CarouselAdapter(ArrayList<Publicacion> dataSet, int height,
                           @NonNull RecyclerViewOnItemClickListener
                                   onItemClickListener) {
        mDataSet = dataSet;
        mHeight = height;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public CarouselAdapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_carousel, parent, false);
        ItemView iv = new ItemView(v);
        return iv;
    }

    @Override
    public void onBindViewHolder(CarouselAdapter.ItemView holder, int position) {
        Publicacion pub = mDataSet.get(position);
        holder.mTitulo.setText(pub.getTitulo());
        holder.mImagen.setImageUrl(pub.getImagen(), imageLoader);
        holder.mImagen.getLayoutParams().height = mHeight;
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitulo;
        private NetworkImageView mImagen;

        public ItemView(View itemView) {
            super(itemView);

            if (imageLoader == null)
                imageLoader = MainApp.getPermission().getImageLoader();

            mImagen = itemView.findViewById(R.id.ivItem);
            mTitulo = itemView.findViewById(R.id.tvTitulo);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }

    public interface  RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}
