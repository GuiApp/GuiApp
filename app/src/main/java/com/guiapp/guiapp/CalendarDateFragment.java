package com.guiapp.guiapp;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.guiapp.guiapp.classes.DataInterchanger;
import com.guiapp.guiapp.classes.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarDateFragment extends DialogFragment {
    private DataInterchanger mDataInterchanger;

    public void setDataInterchanger(DataInterchanger value) {
        this.mDataInterchanger = value;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Date fecha = Utils.stringToDate(this.mDataInterchanger.getStringData());
        final Calendar c = new GregorianCalendar();
        c.setTime(fecha);
        int year =  c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpD = new DatePickerDialog(getActivity(), dateSetListener, year, month, day);

        return dpD;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    month++;
                    String date = year + "-" + month + "-" + day;
                    CalendarDateFragment.this.mDataInterchanger.setStringData(date);
                }
            };

}
