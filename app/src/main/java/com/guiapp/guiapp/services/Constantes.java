package com.guiapp.guiapp.services;


import com.guiapp.guiapp.classes.Utils;

/**
 * Created by usuario on 07/010/2017.
 */

public class Constantes
{
    public static final int CODIGO_DETALLE = 100;
    public static final int CODIGO_ACTUALIZACION = 101;

    private static final String PUERTO_HOST = "";
    private static final String IP = (!Utils.DEBUG) ? ("http://guiappworldwide.com/admin") : ("http://192.168.1.24");
    private static final String API_NAME = (!Utils.DEBUG) ? ("/api/") : ("/api-guiapp/");

    public static final String API_BASE = IP + PUERTO_HOST + API_NAME;
    /**
     * Clave para el valor extra que representa al identificador de una meta
     */
    public static final String EXTRA_ID = "IDEXTRA";

}
