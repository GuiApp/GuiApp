package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class EventoService {
    public static final String API_SECTION = "evento";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/?";
    public static final String GET_LISTADO = Constantes.API_BASE + API_SECTION + "/lista";
    public static final String GET_FILTERED = Constantes.API_BASE + API_SECTION + "/filtered/?1/?2";

    //POST
    public static final String POST = Constantes.API_BASE + API_SECTION;
    public static final String POST_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    //PUT
    public static final String PUT = Constantes.API_BASE + API_SECTION + "/?";
    public static final String PUT_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    public static String getURLGetByID(String idevento) {
        return  GET_BYID.replace("?", idevento);
    }

    public static String getURLGetFiltered(long idtipo, String fecinicio) {
        return GET_FILTERED.replace("?1", idtipo + "").replace("?2", fecinicio);
    }

    public static String getURLPut(String idevento) {
        return PUT.replace("?", idevento);
    }
}
