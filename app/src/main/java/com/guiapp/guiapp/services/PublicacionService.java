package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class PublicacionService {
    public static final String API_SECTION = "publicacion";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_LISTADO = Constantes.API_BASE + API_SECTION;
    public static final String GET_DESTACADAS = Constantes.API_BASE + API_SECTION + "/destacadas";
    public static final String GET_PUBLICACIONES = Constantes.API_BASE + API_SECTION + "/regulares";
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/byid/?";
    public static final String GET_BYIDCUENTA = Constantes.API_BASE + API_SECTION + "/idcuenta/?";
    public static final String GET_BYIDEMPRESA = Constantes.API_BASE + API_SECTION + "/idempresa/?";

    //POST
    public static final String POST_FROMMOBILE = Constantes.API_BASE + API_SECTION + "/frommobile";
    public static final String POST_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    //PUT
    public static final String PUT = Constantes.API_BASE + API_SECTION + "/?";
    public static final String PUT_FROMMOBILE = Constantes.API_BASE + API_SECTION + "/frommobile";
    public static final String PUT_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    //PATCH
    //public static final String PATCH_BYIDCUENTA = Constantes.API_BASE + API_SECTION + "/idcuenta";

    public static String getURLGetByID(String idpublicacion) {
        return GET_BYID.replace("?", idpublicacion);
    }

    public static String getURLGetByIDCuenta(String idcuenta) {
        return GET_BYIDCUENTA.replace("?", idcuenta);
    }

    public static String getURLGetByIDEmpresa(String idempresa) {
        return GET_BYIDEMPRESA.replace("?", idempresa);
    }

    public static String getURLPut(String idpublicacion) {
        return PUT_FROMMOBILE.replace("?", idpublicacion);
    }
}
