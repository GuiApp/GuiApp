package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class FuncionService {
    public static final String API_SECTION = "funcion";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_LISTA = Constantes.API_BASE + API_SECTION;
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/?";
    public static final String GET_BYIDCUENTA = Constantes.API_BASE + API_SECTION + "/getbyidcuenta/?";

    public static String getURLGetByID(int idpaquete) {
        return GET_BYID.replace("?", idpaquete + "");
    }

    public static String getURLGetByIDCuenta(String idcuenta) {
        return GET_BYIDCUENTA.replace("?", idcuenta);
    }
}
