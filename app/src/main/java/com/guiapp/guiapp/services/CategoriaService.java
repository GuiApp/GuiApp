package com.guiapp.guiapp.services;

/**
 * Created by ajade on 22/03/2018.
 */

public class CategoriaService {
    public static final String API_SECTION = "categoria";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GET
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/?";
    public static final String GET_LIST = Constantes.API_BASE + API_SECTION;
}
