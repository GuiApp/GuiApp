package com.guiapp.guiapp.services;

/**
 * Created by ajade on 08/02/2018.
 */

public class PublicidadService {
    public static final String API_SECTION = "publicidad";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_RANDOM = Constantes.API_BASE + API_SECTION + "/r";
}
