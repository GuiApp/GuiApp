package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class EventoTipoService {
    public static final String API_SECTION = "eventotipo";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_LISTA = Constantes.API_BASE + API_SECTION;
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/?";

    public static String getURLGetByID(int idpaquete) {
        return GET_BYID.replace("?", idpaquete + "");
    }
}
