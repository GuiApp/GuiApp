package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class EmpresaService {
    public static final String API_SECTION = "empresa";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GET
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/byid/?";
    public static final String GET_XCUENTA = Constantes.API_BASE + API_SECTION + "/xcuenta/?";
    public static final String GET_LISTA = Constantes.API_BASE + API_SECTION + "/l";
    public static final String GET_CATEGORIAS = Constantes.API_BASE + API_SECTION + "/categorias/?";

    //PATCH
    public static final String PATCH_BUSQUEDA = Constantes.API_BASE + API_SECTION + "/b";
    public static final String PATCH_CATEGORIA = Constantes.API_BASE + API_SECTION + "/c";

    //POST
    public static final String POST_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";
    public static final String POST_CATEGORIAS = Constantes.API_BASE + API_SECTION + "/c";

    //PUT
    public static final String PUT = Constantes.API_BASE + API_SECTION + "/?";
    public static final String PUT_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";
    public static final String PUT_CATEGORIAS = Constantes.API_BASE + API_SECTION + "/c";

    public static String getURLGetByID(String idempresa) {
        return GET_BYID.replace("?", idempresa);
    }

    public static String getURLXCuenta(String idcuenta) {
        return GET_XCUENTA.replace("?", idcuenta);
    }

    public static String getURLCategorias(String idempresa) {
        return GET_CATEGORIAS.replace("?", idempresa);
    }

    public static String getURLPut(String idempresa) {
        return PUT.replace("?", idempresa);
    }
}
