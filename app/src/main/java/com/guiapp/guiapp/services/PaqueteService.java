package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class PaqueteService {
    public static final String API_SECTION = "paquete";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String GET_LISTA = Constantes.API_BASE + API_SECTION + "/l";
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/p/?";
    public static final String GET_BYIDCUENTA = Constantes.API_BASE + API_SECTION + "/c/?";

    //POST
//    public static final String POST_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    //PUT
//    public static final String PUT = Constantes.API_BASE + API_SECTION + "/?";
//    public static final String PUT_IMAGEN = Constantes.API_BASE + API_SECTION + "/imagen";

    public static String getURLGetByID(int idpaquete) {
        return GET_BYID.replace("?", idpaquete + "");
    }

    public static String getURLGetByIDCuenta(String idcuenta) {
        return GET_BYIDCUENTA.replace("?", idcuenta);
    }

//    public static String getURLPut(String idpublicacion) {
//        return PUT.replace("?", idpublicacion);
//    }
}
