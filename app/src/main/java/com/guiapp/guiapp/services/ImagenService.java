package com.guiapp.guiapp.services;

/**
 * Created by ajade on 02/02/2018.
 */

public class ImagenService {
    public static final String API_SECTION = "imagen";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GET
    public static final String GET_BYIDEMPRESA = Constantes.API_BASE + API_SECTION + "/porempresa/?";

    //POST
    public static final String POST = Constantes.API_BASE + API_SECTION;

    //PUT
    public static final String PUT = Constantes.API_BASE + API_SECTION +"/?";

    public static String getURLGetByIDEmpresa(String idempresa) {
        return GET_BYIDEMPRESA.replace("?", idempresa);
    }

    public static String getURLPut(String idimagen) {
        return PUT.replace("?", idimagen);
    }
}
