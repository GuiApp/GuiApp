package com.guiapp.guiapp.services;

/**
 * Created by ajade on 06/02/2018.
 */

public class SucursalService {
    public static final String API_SECTION = "sucursal";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GET
    public static final String GET_LISTIDEMPRESA = Constantes.API_BASE + API_SECTION + "/porempresa/?";
    public static final String GET_BYID = Constantes.API_BASE + API_SECTION + "/byid/?";

    //PUT
    public static final String PUT = Constantes.API_BASE + API_SECTION + "/?";

    public static String getURLListByIdEmpresa(String idempresa) {
        return GET_LISTIDEMPRESA.replace("?", idempresa);
    }

    public static String getURLGetByID(String idsucursal) {
        return GET_BYID.replace("?", idsucursal);
    }

    public static String getURLPut(String idpublicacion) {
        return PUT.replace("?", idpublicacion);
    }
}
