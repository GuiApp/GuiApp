package com.guiapp.guiapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.PublicacionesAdapter;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.entities.Publicacion;
import com.guiapp.guiapp.services.PublicacionService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PublicacionesActivity extends AppCompatActivity implements View.OnClickListener {

    private PublicacionesAdapter.RecyclerViewOnItemClickListener mItemOnClickListener;
    private ArrayList<Publicacion> marrPublicaciones;
    private RecyclerView mPublicaciones;
    private RecyclerView.Adapter madpPublicaciones;
    private RecyclerView.LayoutManager mlmPublicaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicaciones);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;

        setOnClickListenersIM(R.id.btnVolver);
        setOnClickListenersIM(R.id.btnHome);
        setOnClickListenersIM(R.id.btnBuscar);
        setOnClickListenersIM(R.id.btnAgregar);

        mlmPublicaciones = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        mItemOnClickListener = new PublicacionesAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Publicacion pub = marrPublicaciones.get(position);
                SessionProcessor.setIdPublicacion(PublicacionesActivity.this, pub.getId());
                Intent intent = new Intent(PublicacionesActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
            }
        };
        mPublicaciones = findViewById(R.id.rvPublicaciones);
        mPublicaciones.setHasFixedSize(true);
        mPublicaciones.setLayoutManager(mlmPublicaciones);
        marrPublicaciones = new ArrayList<Publicacion>();
        madpPublicaciones = new PublicacionesAdapter(marrPublicaciones, screenWidth, screenHeight, mItemOnClickListener);
        mPublicaciones.setAdapter(madpPublicaciones);

        getPublicaciones();
    }

    private void setOnClickListenersIM(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnVolver:
                intent = new Intent(PublicacionesActivity.this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnHome:
                intent = new Intent(PublicacionesActivity.this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                intent = new Intent(PublicacionesActivity.this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(PublicacionesActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    private void getPublicaciones() {
        String idempresa = SessionProcessor.getIdEmpresa(this);
        if(idempresa.isEmpty()) {
            showMessage("Ingresó como invitado, por lo tanto no posee publicaciones.", true);
            return;
        }

        String customUrl = PublicacionService.getURLGetByIDEmpresa(idempresa);

        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        JSONArray arr = response;
                        marrPublicaciones.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Publicacion pub = new Publicacion();
                                pub.setId(obj.getString("id"));
                                pub.setIdempresa(obj.getString("idempresa"));
                                pub.setTitulo(obj.getString("titulo"));
                                pub.setImagen(obj.getString("imagen"));
                                pub.setDescripcion(obj.getString("descripcion"));
                                pub.setDestacada(obj.getInt("destacada"));
                                pub.setFecmodificacion(obj.getString("fecmodificacion"));
                                pub.setDirty(obj.getInt("dirty"));
                                pub.setIdcuenta(obj.getString("idcuenta"));
                                marrPublicaciones.add(pub);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if(marrPublicaciones.size() == 0) {
                            showMessage("No posee publicaciones registradas. Por favor agregue una.", true);
                        }

                        madpPublicaciones.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(PublicacionesActivity.this);
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Sin conexión!!");
                alert.show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void showMessage(String mensaje, Boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Guiapp");
        if(!salir) {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        } else {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    Intent intent = new Intent(PublicacionesActivity.this, EdicionPublicacionActivity.class);
                    SessionProcessor.setIdPublicacion(PublicacionesActivity.this, "");
                    startActivity(intent);
                    finish();
                }
            });
        }
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }
}
