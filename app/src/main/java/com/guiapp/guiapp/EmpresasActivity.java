package com.guiapp.guiapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.adapters.CategoriasSpinAdapter;
import com.guiapp.guiapp.adapters.EmpresasSpinAdapter;
import com.guiapp.guiapp.adapters.SucursalesEmpresaAdapter;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.CategoriaXEmpresa;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Empresa;
import com.guiapp.guiapp.entities.Funcion;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.enums.Categorias;
import com.guiapp.guiapp.enums.Modulos;
import com.guiapp.guiapp.services.EmpresaService;
import com.guiapp.guiapp.services.FuncionService;
import com.guiapp.guiapp.services.SucursalService;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EmpresasActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PICK_IMAGE_REQUEST= 99;

    ImageView mLogo;
    Spinner mspnEmpresa, mCategorias;
    ListView mSucursales;
    ImageButton mbtnVolver;
    Button mbtnGuardar, mbtnCancelar;
    ImageButton mbtnPublicaciones, mbtnCalendario, mbtnNuevaSuc;
    Bitmap mBitmap;

    EmpresasSpinAdapter madpEmpresas;
    CategoriasSpinAdapter madpCategorias;
    SucursalesEmpresaAdapter madpSucursales;

    ArrayList<Empresa> marrEmpresas;
    ArrayList<Categorias> marrCategorias;
    ArrayList<Sucursal> marrSucursales;
    String mLogoPath;
    Boolean mFromChoosePhoto;
    Boolean mReloadData;

    boolean mLimiAlcanzado;

    Empresa mEmpresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        if(Build.VERSION.SDK_INT >= 23) {
            if(!checkPermission())
                requestPermission();
        }

        mReloadData = true;
        mFromChoosePhoto = false;
        mLogoPath = "";
    }

    @Override
    protected void onResume(){
        super.onResume();

        MainApp.setSucursal(null);

        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            showMessage("Con cuenta invitado no puede agregar empresas. Por favor regístrese. Cierre la app para volver a intentarlo.", true);
            return;
        }

        getFunciones();

        if(mFromChoosePhoto) {
            mFromChoosePhoto = false;
            return;
        }

        if(!mReloadData && mspnEmpresa != null)
            return;

        setHintText(R.id.etRazonSocial, "Denominación / Razón Social");
        setHintText(R.id.etCuit, "CUIT");
        setHintText(R.id.etMedioPago, "Medios de pago");
        setHintText(R.id.etEstado, "Estado");
        setHintText(R.id.etFacebook, "Facebook");
        setHintText(R.id.etInstagram, "Instagram");
        setHintText(R.id.etTwitter, "Twitter");
        setHintText(R.id.etPalabrasClaves, "Palabras claves");

        mLogo = findViewById(R.id.ivLogo);
        mspnEmpresa = findViewById(R.id.spEmpresa);
        mSucursales = findViewById(R.id.lvSucursales);
        mCategorias = findViewById(R.id.spCategoria);

        mbtnVolver = findViewById(R.id.btnVolver);
        mbtnGuardar = findViewById(R.id.btnGuardar);
        mbtnCancelar = findViewById(R.id.btnCancelar);
        mbtnPublicaciones = findViewById(R.id.btnPublicaciones);
        mbtnCalendario = findViewById(R.id.btnCalendario);
        mbtnNuevaSuc = findViewById(R.id.btnNuevaSuc);

        mLogo.setOnClickListener(this);
        setOnClickListeners(R.id.btnVolver, false);
        setOnClickListeners(R.id.btnGuardar, true);
        setOnClickListeners(R.id.btnCancelar, true);
        setOnClickListeners(R.id.btnPublicaciones, false);
        setOnClickListeners(R.id.btnCalendario, false);
        setOnClickListeners(R.id.btnNuevaSuc, false);
        setOnClickListeners(R.id.btnGalería, false);

        marrEmpresas = new ArrayList<>();
        madpEmpresas = new EmpresasSpinAdapter(this, R.layout.spinner_item, marrEmpresas);
        madpEmpresas.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mspnEmpresa.setAdapter(madpEmpresas);
        mspnEmpresa.setOnItemSelectedListener(this);

        marrCategorias = Categorias.getArray();
        madpCategorias = new CategoriasSpinAdapter(this, R.layout.spinner_item, marrCategorias);
        madpCategorias.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mCategorias.setAdapter(madpCategorias);

        marrSucursales = new ArrayList<>();
        madpSucursales = new SucursalesEmpresaAdapter(this, R.id.lvSucursales, marrSucursales);
        mSucursales.setAdapter(madpSucursales);
        mSucursales.setDividerHeight(2);
        mSucursales.setDivider(this.getResources().getDrawable(R.drawable.divider_color));
        mSucursales.setOnItemClickListener(new ItemClickListener());

        mspnEmpresa.setVisibility(View.VISIBLE);
        getEmpresas(SessionProcessor.getIdEmpresa(EmpresasActivity.this));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                super.onBackPressed();
                break;
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnCancelar:
                intent = new Intent(EmpresasActivity.this, EmpresasActivity.class);
                startActivity(intent);
                break;
            case R.id.ivLogo:
                showFileChooser();
                break;
            case R.id.btnPublicaciones:
                SessionProcessor.setIdEmpresa(this, mEmpresa.getId());
                intent = new Intent(EmpresasActivity.this, PublicacionesActivity.class);
                startActivity(intent);
                break;
            case R.id.btnCalendario:
                newEvento();
                break;
            case R.id.btnNuevaSuc:
                newSucursal();
                break;
            case R.id.btnGalería:
                displayGaleria();
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mReloadData = false;
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            assert resultUri != null;
            File f = new File(resultUri.getPath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = true;
            mBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
            mLogoPath = resultUri.getPath();
            mFromChoosePhoto = true;
            Picasso.get().load(f).fit().into(mLogo);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            assert cropError != null;
            Utils.showMessage(this,cropError.getMessage(), false);
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            String tempname = 'e' + UUID.randomUUID().toString();
            UCrop.of(filePath, Uri.fromFile(new File(getCacheDir(), tempname)))
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(225, 250)
                    .start(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(marrEmpresas.get(i).getId().isEmpty() && !mFromChoosePhoto) {
            prepareForNew();
            mFromChoosePhoto = false;
            return;
        }
        mFromChoosePhoto = false;
        getSucursalesByIdEmpresa(marrEmpresas.get(i).getId());
        getEmpresaData(marrEmpresas.get(i).getId());
        getCategoriasXEmpresas(marrEmpresas.get(i).getId());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void newSucursal() {
        Funcion funcion = new Funcion();
        funcion.loadPermisos(this);
        mLimiAlcanzado = funcion.getLimAlcanzado(Modulos.sucursal);
        if(mLimiAlcanzado) {
            showMessage("No puede crear nuevas sucursales. Consulte nuestros paquetes Premium!!", false);
        } else {
            SessionProcessor.setIdEmpresa(this, mEmpresa.getId());
            SessionProcessor.setIdSucursal(this, "");
            startActivity(new Intent(EmpresasActivity.this, SucursalActivity.class));
        }
    }

    private void newEvento() {
        Funcion funcion = new Funcion();
        funcion.loadPermisos(this);
        mLimiAlcanzado = funcion.getLimAlcanzado(Modulos.eventos);
        if(mLimiAlcanzado)
            showMessage("No puede crear nuevos eventos", false);
        else {
            SessionProcessor.setIdEmpresa(this, mEmpresa.getId());
            SessionProcessor.setIdEvento(this, "");
            startActivity(new Intent(EmpresasActivity.this, EventoABMActivity.class));
        }
    }

    private void displayGaleria() {
        SessionProcessor.setIdEmpresa(this, mEmpresa.getId());
        startActivity(new Intent(EmpresasActivity.this, GaleriaActivity.class));
    }

    private void prepareForNew() {

        Funcion funcion = new Funcion();
        funcion.loadPermisos(this);
        mLimiAlcanzado = funcion.getLimAlcanzado(Modulos.empresa);
        if(mLimiAlcanzado) {
            showMessage("No puede crear nuevas empresas. Consulte nuestros paquetes Premium!!", false);
            return;
        }

        mEmpresa = new Empresa();
        SessionProcessor.setIdEmpresa(EmpresasActivity.this, "");
        loadLogo();

        setTextos(R.id.etRazonSocial, "");
        setTextos(R.id.etCuit, "");
        setTextos(R.id.etMedioPago, "");
        setTextos(R.id.etEstado, "");
        setTextos(R.id.etFacebook, "");
        setTextos(R.id.etInstagram, "");
        setTextos(R.id.etTwitter, "");
        setTextos(R.id.etPalabrasClaves, "");

        setHintText(R.id.etRazonSocial, "Denominación / Razón Social");
        setHintText(R.id.etCuit, "CUIT");
        setHintText(R.id.etMedioPago, "Medios de pago");
        setHintText(R.id.etEstado, "Me fuí a comer, vuelvo en 5 minutos");
        setHintText(R.id.etFacebook, "katyperry");
        setHintText(R.id.etInstagram, "katyperry");
        setHintText(R.id.etTwitter, "katyperry");
        setHintText(R.id.etPalabrasClaves, "café, vino, odontólogo");

        mbtnNuevaSuc.setEnabled(false);

        marrSucursales.clear();
        madpSucursales.notifyDataSetChanged();
        mspnEmpresa.setFocusable(true);
    }

    private void verifyLimite(boolean showMess) {
        String idempresa = SessionProcessor.getIdEmpresa(this);
        if(!idempresa.isEmpty())
            return;

        Funcion funcion = new Funcion();
        funcion.loadPermisos(this);
        mLimiAlcanzado = funcion.getLimAlcanzado(Modulos.empresa);
        madpEmpresas.setDisableNew(mLimiAlcanzado);

        if(mLimiAlcanzado && showMess) {
            showMessage("No puede crear nuevas empresas. Consulte nuestros paquetes Premium!!", true);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        mFromChoosePhoto = true;
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), PICK_IMAGE_REQUEST);
    }

    private void guardar() {
        mspnEmpresa.setVisibility(View.VISIBLE);

        if (mEmpresa.getId().isEmpty())
            post();
        else
            put();
    }

    private void setHintText(int id, String s) {
        EditText edittext = findViewById(id);
        edittext.setEnabled(true);
        Spanned text = Html.fromHtml("<i>" + s + "</i>");
        edittext.setHint(text);
    }

    private String getTextFromTV(int id) {
        EditText edittext = findViewById(id);
        return edittext.getText().toString();
    }

    private void setOnClickListeners(int view, boolean isButton) {
        if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(EmpresasActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EmpresasActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(EmpresasActivity.this, " Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(EmpresasActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private void getEmpresas(final String idToSelect) {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            showMessage("No posee empresas registras a la cuales asociar la publicación. Por favor registre una empresa.", true);
            return;
        }

        String customUrl = EmpresaService.getURLXCuenta(idcuenta);

        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int posSelected = 0;
                        Empresa empresa = new Empresa();
                        empresa.setRazonsocial("Nueva empresa");
                        marrEmpresas.clear();
                        marrEmpresas.add(empresa);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                empresa = new Empresa();
                                empresa.setId(obj.getString("id"));
                                empresa.setRazonsocial(obj.getString("razonsocial"));
                                empresa.setCuit(obj.getLong("cuit"));
                                empresa.setMediodepago(obj.getString("mediodepago"));
                                empresa.setEstado(obj.getString("estado"));
                                empresa.setLogo(obj.getString("logo"));
                                empresa.setFacebook(obj.getString("facebook"));
                                empresa.setTwitter(obj.getString("twitter"));
                                empresa.setInstagram(obj.getString("instagram"));
                                empresa.setPalabrasclave(obj.getString("palabrasclave"));
                                empresa.setDirty(obj.getInt("dirty"));
                                empresa.setDescripcion(obj.getString("descripcion"));
                                empresa.setIdcuenta(obj.getString("idcuenta"));

                                if(empresa.getId().equals(idToSelect))
                                    posSelected = i;
                                marrEmpresas.add(empresa);
                                mbtnNuevaSuc.setEnabled(true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if(marrEmpresas.size() == 0) {
                            showMessage("No posee empresas registradas. Por favor agregue una empresa.", true);
                        } else {
                            mspnEmpresa.setSelection(posSelected + 1);
                        }

                        madpEmpresas.notifyDataSetChanged();
                        verifyLimite(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("Sin conexión!!", false);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getCategoriasXEmpresas(final String idToSelect) {

        String customUrl = EmpresaService.getURLCategorias(idToSelect);

        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        CategoriaXEmpresa cxe;
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                cxe = new CategoriaXEmpresa();
                                cxe.setIdcategoria(obj.getInt("idcategoria"));
                                mCategorias.setSelection(cxe.getIdcategoria());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("Nose pudo obtener la categoria", false);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        SessionProcessor.setIdEmpresa(EmpresasActivity.this, mEmpresa.getId());
//        if(mFromChoosePhoto1) {
//            mFromChoosePhoto1 = false;
//        }else
        loadLogo();

        setTextos(R.id.etRazonSocial, mEmpresa.getRazonsocial());
        setTextos(R.id.etCuit, mEmpresa.getCuit() +"");
        setTextos(R.id.etMedioPago, mEmpresa.getMediodepago());
        setTextos(R.id.etEstado, mEmpresa.getEstado());

        String value = mEmpresa.getFacebook().replace("m.facebook.com/", "").replace("www.facebook.com/", "");
        setTextos(R.id.etFacebook, value);
        value = mEmpresa.getInstagram().replace("www.instagram.com/", "");
        setTextos(R.id.etInstagram, value);
        value = mEmpresa.getTwitter().replace("www.twitter.com/", "");
        setTextos(R.id.etTwitter, value);
        setTextos(R.id.etPalabrasClaves, mEmpresa.getPalabrasclave());

        mbtnNuevaSuc.setEnabled(true);

        //mCategorias.setSelection(mEmpresa.get);
    }

    private void loadLogo() {
        if(!URLUtil.isValidUrl(mEmpresa.getLogo())) {
            Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.logoempresa);
            mLogo.setImageBitmap(logo);
        } else {
//            Picasso.with(EmpresasActivity.this).invalidate(mEmpresa.getLogo());
            Picasso.get().load(mEmpresa.getLogo()).into(mLogo);
        }
    }

    private void setTextos(int id, String text) {
        EditText et = findViewById(id);
        et.setText(text);
    }

    private void showMessage(String mensaje, Boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Guiapp");
        if(!salir) {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        } else {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    Intent intent = new Intent(EmpresasActivity.this, DashboardActivity.class);
                    startActivity(intent);
                }
            });
        }
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }

    private void getSucursalesByIdEmpresa(String idempresa) {
        if(idempresa.isEmpty())
            return;

        String customUrl = SucursalService.getURLListByIdEmpresa(idempresa);

        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrSucursales.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Sucursal sucursal = new Sucursal();
                                sucursal.setId(obj.getString("id"));
                                sucursal.setIdempresa(obj.getString("idempresa"));
                                sucursal.setRazonsocial(obj.getString("razonsocial"));
                                sucursal.setDireccion(obj.getString("direccion"));
                                marrSucursales.add(sucursal);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if(marrSucursales.size() == 0) {
                            showMessage("No posee sucursales registradas. Es neceario para mostrar la dirección.", false);
                        }

                        madpSucursales.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("Sin conexión!!", false);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getEmpresaData(String idempresa) {
        if(idempresa.isEmpty())
            return;

        String customUrl = EmpresaService.getURLGetByID(idempresa);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mEmpresa = new Empresa();
                            mEmpresa.setId(obj.getString("id"));
                            mEmpresa.setRazonsocial(obj.getString("razonsocial"));
                            mEmpresa.setCuit(obj.getLong("cuit"));
                            mEmpresa.setMediodepago(obj.getString("mediodepago"));
                            mEmpresa.setEstado(obj.getString("estado"));
                            mEmpresa.setLogo(obj.getString("logo"));
                            mEmpresa.setFacebook(obj.getString("facebook"));
                            mEmpresa.setTwitter(obj.getString("twitter"));
                            mEmpresa.setInstagram(obj.getString("instagram"));
                            mEmpresa.setPalabrasclave(obj.getString("palabrasclave"));
                            mEmpresa.setDirty(obj.getInt("dirty"));
                            mEmpresa.setWeb(obj.getString("web"));
                            mEmpresa.setDescripcion(obj.getString("descripcion"));
                            mEmpresa.setIdcuenta(obj.getString("idcuenta"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder add = new AlertDialog.Builder(EmpresasActivity.this);
                        add.setMessage(error.getMessage()).setCancelable(true);
                        AlertDialog alert = add.create();
                        alert.setTitle("Sin conexión!!");
                        alert.show();
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void post() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        mEmpresa.setIdcuenta(idcuenta);
        String value = "m.facebook.com/" + getTextFromTV(R.id.etFacebook);
        mEmpresa.setFacebook(value);
        mEmpresa.setMediodepago(getTextFromTV(R.id.etMedioPago));
        mEmpresa.setRazonsocial(getTextFromTV(R.id.etRazonSocial));
        mEmpresa.setEstado(getTextFromTV(R.id.etEstado));
        value = (!getTextFromTV(R.id.etCuit).isEmpty()) ? getTextFromTV(R.id.etCuit) : "0";
        mEmpresa.setCuit(Long.parseLong(value));
        mEmpresa.setDirty(0);
        value = "www.instagram.com/" + getTextFromTV(R.id.etInstagram);
        mEmpresa.setInstagram(value);
        mEmpresa.setPalabrasclave(getTextFromTV(R.id.etPalabrasClaves));
        value = "www.twitter.com/" + getTextFromTV(R.id.etTwitter);
        mEmpresa.setTwitter(value);
        mEmpresa.setLogo("");

        if(mEmpresa.getRazonsocial().isEmpty()
                || mEmpresa.getCuit() < 0
                || mEmpresa.getEstado().isEmpty()
                || mEmpresa.getPalabrasclave().isEmpty()) {
            showMessage("Faltan completar datos!!", false);
            return;
        }

        mEmpresa.setId(UUID.randomUUID().toString());
        HashMap<String, String> map = mEmpresa.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EmpresaService.API_CALL;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showMessage("Guardado! Muy pronto tu negocio se dará de alta!", false);
                        postImagen();
                        postCategoria();
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                        getFunciones();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("La empresa no se ha podido guardar!! \n Por favor reintente.", false);
                        mEmpresa.setId("");
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void postCategoria() {
        CategoriaXEmpresa cxe = new CategoriaXEmpresa();
        cxe.setIdcategoria(mCategorias.getSelectedItemPosition());
        cxe.setIdempresa(mEmpresa.getId());

        HashMap<String, String> map = cxe.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EmpresaService.POST_CATEGORIAS;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La empresa se ha guardado con éxito!!", false);
                        //postImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                        getFunciones();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("Las categorias de la empresa no se han podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getFunciones() {
        DatabaseHelper dBHelper = DatabaseHelper.getInstance(this);
        SQLiteDatabase mDatabase = dBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

//        if(cuenta.getSync() == 1)
//            return;

        String url = FuncionService.getURLGetByIDCuenta(cuenta.getId());
        JsonArrayRequest jsonreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        SessionProcessor.setFunciones(EmpresasActivity.this, response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Utils.DEBUG)
                    Toast.makeText(EmpresasActivity.this, "Error: getFunciones",Toast.LENGTH_SHORT).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void put() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        mEmpresa.setIdcuenta(idcuenta);
        String value = "m.facebook.com/" + getTextFromTV(R.id.etFacebook);
        mEmpresa.setFacebook(value);
        mEmpresa.setMediodepago(getTextFromTV(R.id.etMedioPago));
        mEmpresa.setRazonsocial(getTextFromTV(R.id.etRazonSocial));
        mEmpresa.setEstado(getTextFromTV(R.id.etEstado));
        value = (!getTextFromTV(R.id.etCuit).isEmpty()) ? getTextFromTV(R.id.etCuit) : "0";
        mEmpresa.setCuit(Long.parseLong(value));
        mEmpresa.setDirty(0);
        value = "www.instagram.com/" + getTextFromTV(R.id.etInstagram);
        mEmpresa.setInstagram(value);
        mEmpresa.setPalabrasclave(getTextFromTV(R.id.etPalabrasClaves));
        value = "www.twitter.com/" + getTextFromTV(R.id.etTwitter);
        mEmpresa.setTwitter(value);

        if(mEmpresa.getRazonsocial().isEmpty()
                || mEmpresa.getCuit() < 0
                || mEmpresa.getEstado().isEmpty()
                || mEmpresa.getPalabrasclave().isEmpty()) {
            showMessage("Faltan completar datos!!", false);
            return;
        }

        HashMap<String, String> map = mEmpresa.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EmpresaService.getURLPut(mEmpresa.getId());

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showMessage("La empresa se ha guardado con éxito!!", false);
                        putCategoria();
                        putImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("La empresa no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void putCategoria() {
        CategoriaXEmpresa cxe = new CategoriaXEmpresa();
        cxe.setIdcategoria(mCategorias.getSelectedItemPosition());
        cxe.setIdempresa(mEmpresa.getId());

        HashMap<String, String> map = cxe.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EmpresaService.PUT_CATEGORIAS;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Grabadas categorias x emprea con éxito", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("Las categorias por empresa no se han podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void putImagen() {
        if(mLogoPath.isEmpty())
            return;

        String customUrl = EmpresaService.PUT_IMAGEN;
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mEmpresa.getId());
        param.put("image",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                        getEmpresas(SessionProcessor.getIdEmpresa(EmpresasActivity.this));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    public void postImagen(){

        if(mLogoPath.isEmpty())
            return;

        String customUrl = EmpresaService.POST_IMAGEN;
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mEmpresa.getId());
        param.put("image",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        if (Utils.DEBUG)
                            Toast.makeText(EmpresasActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                        getEmpresas(mEmpresa.getId());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    public String getStringImage(Bitmap bitmap){
        Log.i("MyHitesh",""+bitmap);
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp.replace("\n", "");
    }

    private class ItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent();
            Sucursal suc = marrSucursales.get(i);
            SessionProcessor.setIdEmpresa(EmpresasActivity.this, suc.getIdempresa());
            SessionProcessor.setIdSucursal(EmpresasActivity.this, suc.getId());
            intent.setClass(EmpresasActivity.this, SucursalActivity.class);
            startActivity(intent);
        }
    }
}
