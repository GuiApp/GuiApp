package com.guiapp.guiapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.adapters.EventoTipoSpinAdapter;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.EventoTipo;
import com.guiapp.guiapp.services.EventoService;
import com.guiapp.guiapp.services.EventoTipoService;
import com.guiapp.guiapp.services.FuncionService;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class EventoABMActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_IMAGE_REQUEST= 99;

    private Evento mEvento;
    ArrayList<EventoTipo> marrTiposEventos;
    EventoTipoSpinAdapter madpTiposEventos;

    ImageView mImagen;
    Spinner mTiposEventos;
    Boolean mFromChoosePhoto;
    Boolean mReloadData;
    Bitmap mBitmap;
    String mLogoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento_abm);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        mReloadData = false;
        mFromChoosePhoto = false;

        marrTiposEventos = new ArrayList<>();
        madpTiposEventos = new EventoTipoSpinAdapter(this, R.layout.spinner_item, marrTiposEventos);
        madpTiposEventos.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mTiposEventos = findViewById(R.id.spTipo);
        mTiposEventos.setAdapter(madpTiposEventos);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(marrTiposEventos.size() == 0)
            getTipoEvento();

        if(mFromChoosePhoto) {
            mFromChoosePhoto = false;
            return;
        }

        if (!SessionProcessor.getIdEvento(this).isEmpty()) {
            getEventoData();
        } else {
            prepareForNew();
        }

        mImagen = findViewById(R.id.ivImagen);
        TextView tvTag = findViewById(R.id.tvTag);
        tvTag.setOnClickListener(this);
        setOnClickListeners(R.id.btnVolver, false, false);
        setOnClickListeners(R.id.ivImagen, false, true);
        setOnClickListeners(R.id.ibObtenerFecha, false, false);
        setOnClickListeners(R.id.ibObtenerHora, false, false);
        setOnClickListeners(R.id.btnGuardar, true, false);
        setOnClickListeners(R.id.btnHome, false, false);
        setOnClickListeners(R.id.btnBuscar, false, false);
        setOnClickListeners(R.id.btnAgregar, false, false);
        setOnClickListeners(R.id.btnConfig, false, false);
        setETOnClickListeners(R.id.tvTag, true);
        setETOnClickListeners(R.id.etFechaInicio, false);
        setETOnClickListeners(R.id.etHorarioInicio, false);

        showKeyboard(R.id.etTitulo);
    }

    private void showKeyboard(int view) {
        EditText yourEditText= findViewById(view);
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void prepareForNew() {
        mEvento = new Evento();
        String idempresa = SessionProcessor.getIdEmpresa(this);
        mEvento.setIdempresa(idempresa);
        mLogoPath="";
    }

    private void getTipoEvento() {
        String customUrl = EventoTipoService.GET_LISTA;
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrTiposEventos.clear();
                        EventoTipo tipo = new EventoTipo();
                        tipo.setId(-1);
                        tipo.setTipo("Seleccionar");
                        marrTiposEventos.add(tipo);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                tipo = new EventoTipo();
                                tipo.setId(obj.getInt("id"));
                                tipo.setTipo(obj.getString("tipo"));
                                marrTiposEventos.add(tipo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        madpTiposEventos.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(EventoABMActivity.this, "Sin conexión!!", Toast.LENGTH_LONG).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getEventoData() {
        String idEvento = SessionProcessor.getIdEvento(this);

        String customUrl = EventoService.getURLGetByID(idEvento);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mEvento = new Evento();
                            mEvento.setId(obj.getString("id"));
                            mEvento.setNombre(obj.getString("nombre"));
                            mEvento.setDescripcion(obj.getString("descripcion"));
                            mEvento.setImagen(obj.getString("imagen"));
                            mEvento.setIdtipo(obj.getLong("idtipo"));
                            mEvento.setTipo(obj.getString("tipo"));
                            mEvento.setUbicacion(obj.getString("ubicacion"));
                            mEvento.setFecinicio(obj.getString("fecinicio"));
                            mEvento.setFecfin(obj.getString("fecfin"));
                            mEvento.setIdempresa(obj.getString("idempresa"));
                            mEvento.setRazonsocial(obj.getString("razonsocial"));
                            mEvento.setHorainicio(obj.getString("horainicio"));
                            mEvento.setHorafin(obj.getString("horafin"));

                            mLogoPath = "";
                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(EventoABMActivity.this, error.getMessage(), false);
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        try {
            Uri.parse(mEvento.getImagen());
        } catch (Exception ignored) {}

        for(int i = 0; i < marrTiposEventos.size(); i++) {
            if(mEvento.getIdtipo() == marrTiposEventos.get(i).getId()) {
                mTiposEventos.setSelection(i);
                break;
            }
        }

        setEdiTextData(R.id.etTitulo, mEvento.getNombre());
        setEdiTextData(R.id.etDescripcion, mEvento.getDescripcion());
        setEdiTextData(R.id.etUbicacion, mEvento.getUbicacion());
        setEdiTextData(R.id.etFechaInicio, mEvento.getFormatedFecinicio());
        String[] hora =  mEvento.getHorainicio().split(":");
        setEdiTextData(R.id.etHorarioInicio, hora[0] + ":" + hora[1]);

        loadImagen();
    }

    private void loadImagen() {
        if(!URLUtil.isValidUrl(mEvento.getImagen())) {
            Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.default_evento);
            mImagen.setImageBitmap(logo);
        } else {
//            Picasso.with(EventoABMActivity.this).invalidate(mEvento.getImagen());
            Picasso.get().load(mEvento.getImagen()).fit().into(mImagen);
            hideSeleccionarImagenTV();
        }
    }

    private void setEdiTextData(int id, String text) {
        EditText et = findViewById(id);
        et.setText(text);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        mFromChoosePhoto = true;
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mReloadData = false;
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            assert resultUri != null;
            File f = new File(resultUri.getPath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = true;
            mBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
            mLogoPath = resultUri.getPath();
            mFromChoosePhoto = true;
            Picasso.get().load(f).fit().into(mImagen);
            hideSeleccionarImagenTV();
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            assert cropError != null;
            Utils.showMessage(this,cropError.getMessage(), false);
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            String tempname = 'p' + UUID.randomUUID().toString();
            UCrop.of(filePath, Uri.fromFile(new File(getCacheDir(), tempname)))
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(235, 475)
                    .start(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void hideSeleccionarImagenTV() {
        TextView tvTag = findViewById(R.id.tvTag);
        tvTag.setVisibility(View.INVISIBLE);
    }

    private void obtenerFecha() {
        final Calendar c = new GregorianCalendar();
        if (!mEvento.getFecinicio().isEmpty()) {
            Date fecha = Utils.stringToDate(mEvento.getFecinicio());
            c.setTime(fecha);
        }
        int year =  c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String fecha = String.format(Locale.getDefault(), "%02d-%02d-%04d", dayOfMonth, month, year);

                EventoABMActivity.this.setEdiTextData(R.id.etFechaInicio, fecha);
                fecha = year + "-" + month + "-" + dayOfMonth;
                mEvento.setFecinicio(fecha);
            }
        },year, month, day);
        recogerFecha.show();
    }

    private void obtenerHora(){
        final Calendar c = new GregorianCalendar();
        int hour =  c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        if (!mEvento.getHorainicio().isEmpty()) {
            String[] arrHora = mEvento.getHorainicio().split(":");
            hour = Integer.parseInt(arrHora[0]);
            minute = Integer.parseInt(arrHora[1]);
        }

        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hora = String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minute);
                EventoABMActivity.this.setEdiTextData(R.id.etHorarioInicio, hora);
                mEvento.setHorainicio(hora);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hour, minute, true);

        recogerHora.show();
    }

    private void guardar() {
        if (mEvento.getId().isEmpty())
            post();
        else
            put();
    }

    private void post() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        mEvento.setIdcuenta(idcuenta);
        mEvento.setDescripcion(getTextFromTV(R.id.etDescripcion));
        mEvento.setNombre(getTextFromTV(R.id.etTitulo));
        mEvento.setImagen("");
        mEvento.setIdtipo(mTiposEventos.getSelectedItemId());
        mEvento.setUbicacion(getTextFromTV(R.id.etUbicacion));
        mEvento.setFecfin("");
        mEvento.setHorafin("");
        String idempresa = SessionProcessor.getIdEmpresa(this);
        mEvento.setIdempresa(idempresa);

        if(mLogoPath.isEmpty()) {
            Utils.showMessage(EventoABMActivity.this, "Falta imagen!!", false);
            return;
        }

        if(mEvento.getNombre().isEmpty()
                || mEvento.getDescripcion().isEmpty()
                || mEvento.getIdtipo() < 1
                || mEvento.getIdcuenta().isEmpty()
                || mEvento.getUbicacion().isEmpty()
                || mEvento.getIdempresa().isEmpty()
                || mEvento.getFecinicio().isEmpty()
                || mEvento.getHorainicio().isEmpty()) {
            Utils.showMessage(EventoABMActivity.this, "Faltan completar datos!!", false);
            return;
        }
        mEvento.setId(UUID.randomUUID().toString());

        HashMap<String, String> map = mEvento.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EventoService.POST;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.showMessage(EventoABMActivity.this, "El evento se ha guardado con éxito!!", false);
                        postImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                        getFunciones();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mEvento.setId("");
                        Utils.showMessage(EventoABMActivity.this, "El evento no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    public void postImagen(){

        if(mLogoPath.isEmpty())
            return;

        String customUrl = EventoService.POST_IMAGEN;
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mEvento.getId());
        param.put("image",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(EventoABMActivity.this, "La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void put() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        mEvento.setIdcuenta(idcuenta);
        mEvento.setDescripcion(getTextFromTV(R.id.etDescripcion));
        mEvento.setNombre(getTextFromTV(R.id.etTitulo));
        mEvento.setIdtipo(mTiposEventos.getSelectedItemId());
        mEvento.setUbicacion(getTextFromTV(R.id.etUbicacion));
        mEvento.setFecfin("");
        mEvento.setHorafin("");
        String idempresa = SessionProcessor.getIdEmpresa(this);
        mEvento.setIdempresa(idempresa);

        if(mEvento.getDescripcion().isEmpty()
                || mEvento.getIdtipo() < 1
                || mEvento.getIdcuenta().isEmpty()
                || mEvento.getUbicacion().isEmpty()
                || mEvento.getIdempresa().isEmpty()
                || mEvento.getFecinicio().isEmpty()
                || mEvento.getHorainicio().isEmpty()) {
            Utils.showMessage(EventoABMActivity.this, "Faltan completar datos!!", false);
            return;
        }

        HashMap<String, String> map = mEvento.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = EventoService.getURLPut(mEvento.getId());

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.showMessage(EventoABMActivity.this, "El evento se ha guardado con éxito!!", false);
                        putImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(EventoABMActivity.this, "El evento no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void putImagen() {
        if(mLogoPath.isEmpty())
            return;

        String customUrl = EventoService.PUT_IMAGEN;
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mEvento.getId());
        param.put("image",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        if (Utils.DEBUG)
                            Toast.makeText(EventoABMActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(EventoABMActivity.this, "La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getFunciones() {
        DatabaseHelper dBHelper = DatabaseHelper.getInstance(this);
        SQLiteDatabase mDatabase = dBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getSync() == 1)
            return;

        String url = FuncionService.getURLGetByIDCuenta(cuenta.getId());
        JsonArrayRequest jsonreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        SessionProcessor.setFunciones(EventoABMActivity.this, response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Utils.DEBUG)
                    Toast.makeText(EventoABMActivity.this, "Error: getFunciones",Toast.LENGTH_SHORT).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    public String getStringImage(Bitmap bitmap){
        Log.i("MyHitesh",""+bitmap);
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp.replace("\n", "");
    }

    @NonNull
    private String getTextFromTV(int id) {
        EditText edittext = findViewById(id);
        return edittext.getText().toString();
    }

    private void setOnClickListeners(int view, boolean isButton, boolean isImageView) {
        if(isImageView) {
            ImageView imv = findViewById(view);
            imv.setOnClickListener(this);
        } else if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    private void setETOnClickListeners(int view, boolean isTextview) {
        if (isTextview) {
            TextView tv = findViewById(view);
            tv.setOnClickListener(this);
        } else {
            EditText et = findViewById(view);
            et.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnVolver:
                onBackPressed();
                break;
            case R.id.ivImagen:
                showFileChooser();
                break;
            case R.id.ibObtenerFecha:
                obtenerFecha();
                break;
            case R.id.ibObtenerHora:
                obtenerHora();
                break;
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnHome:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(v);
                openContextMenu(v);
                break;
            case R.id.tvTag:
                showFileChooser();
                break;
            case R.id.etFechaInicio:
                obtenerFecha();
                break;
            case R.id.etHorarioInicio:
                obtenerHora();
                break;

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }
}
