package com.guiapp.guiapp.classes;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.Display;

import com.guiapp.guiapp.DashboardActivity;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by usuario on 07/10/2017.
 */

public class Utils
{
    public static final boolean DEBUG = false;

    public static String longToDate(String lngDate) {
        Date date;
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat dfout = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String out = "No registrada";
        try {
            date = df.parse(String.valueOf(lngDate));
            out = dfout.format(date).toString();
        } catch (ParseException e) {
            throw new RuntimeException("Failed to parse date: ", e);
        }

        return out;
    }

    public static String dateToLong(String lngDate, String format) {
        Date date;
        SimpleDateFormat dfOut = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat dfIn = new SimpleDateFormat(format);
        String out = "No registrada";
        try {
            date = dfIn.parse(String.valueOf(lngDate));
            out = dfOut.format(date).toString();
        } catch (ParseException e) {
            throw new RuntimeException("Failed to parse date: ", e);
        }

        return out;
    }

    public static  Date stringToDate(String dateP) {
        if (dateP.isEmpty())
            return  new Date();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateP);
        } catch (ParseException e) {
            date = new Date();
        }
        return date;
    }

    public static int mod(int x, int y) {
        int result = x % y;
        return result < 0? result + y : result;
    }

    public static String getIdEncoded(String rawid) {
        byte[] data = new byte[0];
        try {
            data = rawid.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(data, Base64.URL_SAFE | Base64.NO_PADDING | Base64.NO_WRAP);
    }

    public static void showMessage(final Context cntx, String mensaje, Boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(cntx);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Guiapp");
        if(!salir) {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        } else {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    Intent intent = new Intent(cntx, DashboardActivity.class);
                    cntx.startActivity(intent);
                }
            });
        }
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }

    public static Point getScreenSize(Context cntxt) {
        Display display;
        Point size = new Point();
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            display = ((Activity)cntxt).getWindowManager().getDefaultDisplay();
            size.set(display.getWidth(), display.getHeight());
        } else {
            display = ((Activity)cntxt).getWindowManager().getDefaultDisplay();
            display.getSize(size);
        }

        return size;
    }
}
