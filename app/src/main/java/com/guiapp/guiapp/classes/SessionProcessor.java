package com.guiapp.guiapp.classes;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ajade on 06/02/2018.
 */

public class SessionProcessor {
    private static final String PREFS_NAME = "com.guiapp.guiapp";

    private static final String IDCUENTA = "IDCUENTA";
    private static final String LATITUD = "LATITUD";
    private static final String LONGITUD = "LONGITUD";
    private static final String PARAMETRO = "PARAMETRO";
    private static final String IDEMPRESA = "IDEMPRESA";
    private static final String IDSUCURSAL = "IDSUCURSAL";
    private static final String IDPUBLICACION = "IDPUBLICACION";
    private static final String ACTIVITY_PREVIA= "ACTIVITY_PREVIA";
    private static final String IDPAQUETE= "IDPAQUETE";
    private static final String FUNCIONES= "FUNCIONES";
    private static final String IDEVENTO = "IDEVENTO";

    public static void setIdCuenta(Context cntxt, String idcuenta) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(IDCUENTA, idcuenta);

        editor.apply();
    }

    public static String getIdCuenta(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(IDCUENTA, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setLatitud(Context cntxt, double latitud) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(LATITUD, String.valueOf(latitud));

        editor.apply();
    }

    public static double getLatitud(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return Double.parseDouble(prefs.getString(LATITUD, "0"));
        }
        catch (Exception e)
        {
            return 0;
        }
    }

    public static void setLongitud(Context cntxt, double longitud) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(LONGITUD, String.valueOf(longitud));

        editor.apply();
    }

    public static double getLongitud(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return Double.parseDouble(prefs.getString(LONGITUD, "0"));
        }
        catch (Exception e)
        {
            return 0;
        }
    }

    public static void setParametro(Context cntxt, String parametro) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(PARAMETRO, String.valueOf(parametro));

        editor.apply();
    }

    public static String getParametro(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(PARAMETRO, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setIdEmpresa(Context cntxt, String idempresa) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(IDEMPRESA, String.valueOf(idempresa));

        editor.apply();
    }

    public static String getIdEmpresa(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(IDEMPRESA, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setIdSucursal(Context cntxt, String idsucursal) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(IDSUCURSAL, String.valueOf(idsucursal));

        editor.apply();
    }

    public static String getIdSucursal(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(IDSUCURSAL, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setIdPublicacion(Context cntxt, String idpublicacion) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(IDPUBLICACION, String.valueOf(idpublicacion));

        editor.apply();
    }

    public static String getIdPublicacion(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(IDPUBLICACION, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setActivityPrevia(Context cntxt, String activity) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(ACTIVITY_PREVIA, String.valueOf(activity));

        editor.apply();
    }

    public static String getActivityPrevia(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return prefs.getString(ACTIVITY_PREVIA, "");
        } catch (Exception e) {
            return "";
        }
    }

    public static void setIdPaquete(Context cntxt, int idpaquete) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(IDPAQUETE, idpaquete );

        editor.apply();
    }

    public static int getIdPaquete(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getInt(IDPAQUETE, -1);
        }
        catch (Exception e)
        {
            return -1;
        }
    }

    public static void setFunciones(Context cntxt, String funciones) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(FUNCIONES, funciones);

        editor.apply();
    }

    public static String getIdFunciones(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(FUNCIONES, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static void setIdEvento(Context cntxt, String idevento) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(IDEVENTO, String.valueOf(idevento));

        editor.apply();
    }

    public static String getIdEvento(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try
        {
            return prefs.getString(IDEVENTO, "");
        }
        catch (Exception e)
        {
            return "";
        }
    }
}
