package com.guiapp.guiapp.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.guiapp.guiapp.entities.Cuenta;

/**
 * Created by usuario on 19/07/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    private static final boolean DEBUG = false;
    // Database Info
    private static final String DATABASE_NAME = "guiapp.db";
    private static final int DATABASE_VERSION = 1;
    private static DatabaseHelper mInstance = null;

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context ctx)
    {
        if (mInstance == null)
        {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    @Override
    public void onConfigure(SQLiteDatabase db)
    {
        super.onConfigure(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.beginTransaction();
        db.execSQL(Cuenta.CREATE_TABLE);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if (oldVersion != newVersion)
        {
            // Simplest implementation is to drop all old tables and recreate them
            db.beginTransaction();
            db.execSQL("DROP TABLE IF EXISTS " + Cuenta.TABLE_NAME);
            db.setTransactionSuccessful();
            db.endTransaction();
            onCreate(db);
        }
    }
}
