package com.guiapp.guiapp.classes;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.ImageLoader;
import com.guiapp.guiapp.entities.Sucursal;

/**
 * Created by ajade on 02/02/2018.
 */

public class MainApp extends Application {

    public static final String TAG = MainApp.class.getSimpleName();

    private RequestQueue queue;
    private ImageLoader ImageLoader;

    private static Sucursal mSucursal;

    private static MainApp controller;

    @Override
    public void onCreate() {
        super.onCreate();
        controller = this;
    }

    public static synchronized MainApp getPermission() {
        return controller;
    }

    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(getApplicationContext());
        }

        return queue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (ImageLoader == null) {
            ImageLoader = new ImageLoader(this.queue,
                    new BitmapCache());
        }
        return this.ImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
// set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (queue != null) {
            queue.cancelAll(tag);
        }
    }

    public static Sucursal getSucursal() {
        return mSucursal;
    }

    public static void setSucursal(Sucursal sucursal) {
        MainApp.mSucursal = sucursal;
    }

}
