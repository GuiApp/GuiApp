package com.guiapp.guiapp.classes;

/**
 * Created by ajade on 05/07/2018.
 */

public interface DataInterchanger {
    void setStringData(String value);
    void setIntData(int value);
    void setLongData(Long value);
    void onChangeData();


    String getStringData();
    long getLongData();
}
