package com.guiapp.guiapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.Auth0;
import com.auth0.android.authentication.AuthenticationAPIClient;
import com.auth0.android.authentication.AuthenticationException;
import com.auth0.android.callback.BaseCallback;
import com.auth0.android.provider.AuthCallback;
import com.auth0.android.provider.WebAuthProvider;
import com.auth0.android.result.Credentials;
import com.auth0.android.result.UserProfile;
import com.guiapp.guiapp.classes.CredentialsManager;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
//    private static final String TAG = LoginActivity.class.getName();

    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;

    private View mProgressView;
    private View mLoginFormView;

    private TextView mBienvenido;
    private TextView micFacebook;
    private TextView micGoogle;
    private TextView micTwitter;
    private Button mFacebook;
    private Button mGoogle;
    private Button mTwitter;

    private Auth0 mAuth0;
    private UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Typeface tf = Typeface.createFromAsset(getAssets(), "dosis_light.ttf");
        TextView mVersion = findViewById(R.id.tvVersion);
        mVersion.setTypeface(tf);
        mVersion.setText((getVersion()));

        mBienvenido = findViewById(R.id.tvBienvenido);
        mBienvenido.setTypeface(tf);

        Button mInvitado = findViewById(R.id.btnInvitado);
        mInvitado.setTypeface(tf);
        mInvitado.setOnClickListener(this);

        tf = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        mFacebook = findViewById(R.id.btnFacebook);
        mFacebook.setOnClickListener(this);
        micFacebook = findViewById(R.id.icFacebook);
        micFacebook.setTypeface(tf);
        mGoogle = findViewById(R.id.btnGoogle);
        mGoogle.setOnClickListener(this);
        micGoogle = findViewById(R.id.icGoogle);
        micGoogle.setTypeface(tf);
        mTwitter = findViewById(R.id.btnTwitter);
        mTwitter.setOnClickListener(this);
        micTwitter = findViewById(R.id.icTwitter);
        micTwitter.setTypeface(tf);

        mLoginFormView = findViewById(R.id.svForm);
        mProgressView = findViewById(R.id.login_progress);

        SessionProcessor.setIdCuenta(this, Cuenta.INVITADO);

        verifyToken();
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.btnFacebook:
                login("facebook");
                break;
            case R.id.btnTwitter:
                login("twitter");
                break;
            case R.id.btnGoogle:
                login("google-oauth2");
                break;
            case R.id.btnInvitado:

                goToDashboard();
                break;
        }
    }

    private void drawForm(boolean logued) {
        if(logued)
        {
            mFacebook.setVisibility(View.INVISIBLE);
            micFacebook.setVisibility(View.INVISIBLE);
            mTwitter.setVisibility(View.INVISIBLE);
            micTwitter.setVisibility(View.INVISIBLE);
            mGoogle.setVisibility(View.INVISIBLE);
            micGoogle.setVisibility(View.INVISIBLE);
        }
        else
        {
            mFacebook.setVisibility(View.VISIBLE);
            micFacebook.setVisibility(View.VISIBLE);
            mTwitter.setVisibility(View.VISIBLE);
            micTwitter.setVisibility(View.VISIBLE);
            mGoogle.setVisibility(View.VISIBLE);
            micGoogle.setVisibility(View.VISIBLE);
        }
    }

    private String getVersion() {
        String versionName = "";
        try {
            final PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = "Versión " + packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    private void verifyToken() {
        String accessToken = CredentialsManager.getCredentials(this).getAccessToken();
        if (accessToken == null) {
            drawForm(false);
            showProgress(false);
        } else {
            goToDashboard();
        }
    }

    private void goToDashboard() {
        Intent target = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(target);
        finish();
    }

    private void login(String connection) {
        showProgress(true);
        mAuth0 = new Auth0(this);
        mAuth0.setOIDCConformant(true);
        WebAuthProvider.init(mAuth0)
                .withScheme("xxx")
                .withScope("openid email profile")
                .withConnection(connection)
                .withAudience("https://guiappworldwide.auth0.com/userinfo")
                .start(LoginActivity.this, new LoginCallBack());
    }

    private class LoginCallBack implements AuthCallback {
        @Override
        public void onSuccess(@NonNull final Credentials credentials)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    CredentialsManager.saveCredentials(LoginActivity.this, credentials);
                    getUserInfo();
//                    verifyToken();
                }
            });
        }

        @Override
        public void onFailure(@NonNull final Dialog dialog)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    //dialog.show();
                    Toast.makeText(LoginActivity.this, "Error: ", Toast.LENGTH_SHORT).show();
                    showProgress(false);
                }
            });
        }

        @Override
        public void onFailure(final AuthenticationException exception)
        {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(LoginActivity.this, "Error: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
                    showProgress(false);
                }
            });
        }
    }

    private void getUserInfo() {
        String accessToken = CredentialsManager.getCredentials(this).getAccessToken();
        mAuth0 = new Auth0(this);
        mAuth0.setOIDCConformant(true);

        AuthenticationAPIClient aClient = new AuthenticationAPIClient(mAuth0);
        assert accessToken != null;
        aClient.userInfo(accessToken)
                .start(new BaseCallback<UserProfile, AuthenticationException>()
                {
                    @Override
                    public void onSuccess(final UserProfile payload)
                    {
                        userProfile = payload;
                        runOnUiThread(new Runnable()
                        {
                            public void run()
                            {
                                if(Utils.DEBUG)
                                    Toast.makeText(LoginActivity.this, "Automatic Login Success", Toast.LENGTH_SHORT).show();
                                String sText = "Bienvenido " + userProfile.getName();
                                mBienvenido.setText(sText);
                                saveUserInfo();
                                //efreshUserInfo();
                                drawForm(true);
                                showProgress(false);
                                goToDashboard();
                            }
                        });
                    }

                    @Override
                    public void onFailure(AuthenticationException error)
                    {
                        runOnUiThread(new Runnable()
                        {
                            public void run()
                            {
                                drawForm(false);
                                if(Utils.DEBUG)
                                    Toast.makeText(LoginActivity.this, "Sesión Expirada, Por favor registrese.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        CredentialsManager.deleteCredentials(LoginActivity.this);
                    }
                });
    }

    private void saveUserInfo() {
        Cuenta cuenta = new Cuenta();
        String id;
        if(!TextUtils.isEmpty(userProfile.getEmail())) {
            id = Utils.getIdEncoded(userProfile.getEmail());
            cuenta.setEmail(userProfile.getEmail());
        } else {
            id = Utils.getIdEncoded(userProfile.getExtraInfo().get("sub").toString());
            cuenta.setEmail("");
        }
        cuenta.setId(id);
        cuenta.setNombre(userProfile.getName());
        cuenta.setProvider(userProfile.getExtraInfo().get("sub").toString());//userProfile.getIdentities().get(0).getConnection());
        cuenta.setNotificaciones(1);
        SessionProcessor.setIdCuenta(this, cuenta.getId());

        try {
            mDBHelper = DatabaseHelper.getInstance(this);
            mDatabase = mDBHelper.getWritableDatabase();
            cuenta.save(mDatabase);
        } catch (Exception e) {
            Log.v(this.getPackageName(), e.getMessage());
        }
        finally {
            if(mDatabase.isOpen()) {
                mDatabase.close();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd(Animator animation)
            {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
