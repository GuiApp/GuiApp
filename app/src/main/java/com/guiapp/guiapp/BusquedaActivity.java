package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Publicidad;
import com.guiapp.guiapp.enums.Categorias;
import com.guiapp.guiapp.services.PublicidadService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BusquedaActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText mParams;
    private Publicidad mPublicidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        SessionProcessor.setLatitud(this,0);
        SessionProcessor.setLongitud(this, 0);

        mParams = findViewById(R.id.etSearch);

        setOnClickListeners(R.id.btnSearch);
        setOnClickListeners(R.id.btnConfBusqueda);
        setOnClickListeners(R.id.btnHome);
        setOnClickListeners(R.id.btnAgregar);
        setOnClickListeners(R.id.btnConfig);
        setOnClickListeners(R.id.btnModa);
        setOnClickListeners(R.id.btnGastronimia);
        setOnClickListeners(R.id.btnSalud);
        setOnClickListeners(R.id.btnEntretenimiento);
        setOnClickListeners(R.id.btnTransporte);
        setOnClickListeners(R.id.btnServicios);
        setOnClickListeners(R.id.btnTurismo);
        setOnClickListeners(R.id.btnCalendario);
        setOnClickListeners(R.id.btnMas);

        ImageView img = findViewById(R.id.ivPublicidad);
        img.setOnClickListener(this);

        mPublicidad = null;
        getPublicdad();
    }

    private void setOnClickListeners(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnSearch:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_PARAMETROS);
                break;
            case R.id.btnConfBusqueda:
                intent = new Intent(BusquedaActivity.this, CercaActivity.class);
                break;
            case R.id.btnModa:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.moda.ordinal());
                break;
            case R.id.btnGastronimia:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.gastronomia.ordinal());
                break;
            case R.id.btnSalud:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.salud.ordinal());
                break;
            case R.id.btnEntretenimiento:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.entretenimiento.ordinal());
                break;
            case R.id.btnTransporte:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.transporte.ordinal());
                break;
            case R.id.btnServicios:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.servicios.ordinal());
                break;
            case R.id.btnTurismo:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.turismo.ordinal());
                break;
            case R.id.btnCalendario:
                intent = new Intent(BusquedaActivity.this, CalendarioActivity.class);
                break;
            case R.id.btnMas:
                SessionProcessor.setParametro(this, mParams.getText().toString());
                intent = new Intent(BusquedaActivity.this, ResultadosActivity.class);
                intent.putExtra(ResultadosActivity.TIPO_BUSQUEDA, ResultadosActivity.POR_CATEGORIA);
                intent.putExtra(ResultadosActivity.IDCATEGORIA, Categorias.mas.ordinal());
                break;
            case R.id.ivPublicidad:
                showPublicidad();
                break;
            case R.id.btnHome:
                intent = new Intent(BusquedaActivity.this, DashboardActivity.class);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(BusquedaActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        SessionProcessor.setIdEmpresa(this, "");
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void getPublicdad() {
        JsonArrayRequest jsonreq = new JsonArrayRequest(PublicidadService.GET_RANDOM,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                mPublicidad = new Publicidad();
                                mPublicidad.setId(obj.getLong("id"));
                                mPublicidad.setIdsponsor(obj.getLong("idsponsor"));
                                mPublicidad.setFecinicio(obj.getString("fecinicio"));
                                mPublicidad.setFecfin(obj.getString("fecfin"));
                                mPublicidad.setImagen(obj.getString("imagen"));
                                mPublicidad.setUrl(obj.getString("url"));

                                ImageView iv = findViewById(R.id.ivPublicidad);
                                Picasso.get()
                                        .load(mPublicidad.getImagen())
                                        .placeholder(R.drawable.publicidad)
                                        .error(R.drawable.publicidad)
                                        .into(iv);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ImageView iv = findViewById(R.id.ivPublicidad);
                iv.setImageResource(R.drawable.publicidad);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    protected void showPublicidad() {

        if(mPublicidad == null) {
            return;
        }

        String url = mPublicidad.getUrl();

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setData(Uri.parse(url));
        } catch (Exception e) {
            AlertDialog.Builder add = new AlertDialog.Builder(BusquedaActivity.this);
            add.setMessage("Información no disponible").setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Facebook");
            alert.show();
        }

        startActivity(i);
    }
}
