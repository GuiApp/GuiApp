package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.ResultadosAdapter;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.services.EmpresaService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultadosActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TIPO_BUSQUEDA = "TIPO";

    public static final int POR_PARAMETROS = 0;
    public static final int POR_CATEGORIA = 1;

    public static final String IDCATEGORIA = "IDCATEGORIA";
//    public static final int CAT_MODA = 0;
//    public static final int CAT_GASTRONOMIA = 1;
//    public static final int CAT_SALUD = 2;
//    public static final int CAT_ENTRETENIMIENTO = 3;
//    public static final int CAT_TRANSPORTE = 4;
//    public static final int CAT_SERVICIOS = 5;
//    public static final int CAT_TURISMO = 6;
//    public static final int CAT_CALENDARIO = 7;
//    public static final int CAT_MAS = 8;

    private ResultadosAdapter madpResultados;
    private ArrayList<Sucursal> marrResultados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        setOnClickListeners(R.id.btnSearch);
        setOnClickListeners(R.id.btnConfBusqueda);
        setOnClickListeners(R.id.btnVolver);
        setOnClickListeners(R.id.btnHome);
        setOnClickListeners(R.id.btnBuscar);
        setOnClickListeners(R.id.btnAgregar);
        setOnClickListeners(R.id.btnConfig);

        ListView resultados = findViewById(R.id.lvResultado);
        marrResultados = new ArrayList<>();
        madpResultados = new ResultadosAdapter(this, R.id.lvResultado, marrResultados);
        resultados.setAdapter(madpResultados);
        resultados.setOnItemClickListener(new ItemClickListener());
        resultados.setDivider(this.getResources().getDrawable(R.drawable.transperent_color));
        resultados.setDividerHeight(2);

        if(getIntent().getIntExtra(TIPO_BUSQUEDA, POR_PARAMETROS) == POR_PARAMETROS)
            searchWithParams("");
        else
            searchByCategory("");
    }

    private void setOnClickListeners(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnSearch:
                EditText params = findViewById(R.id.etSearch);
                if(getIntent().getIntExtra(TIPO_BUSQUEDA, POR_PARAMETROS) == POR_PARAMETROS)
                    searchWithParams(params.getText().toString());
                else
                    searchByCategory(params.getText().toString());
                break;
            case R.id.btnConfBusqueda:
                intent = new Intent(ResultadosActivity.this, CercaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnVolver:
                intent = new Intent(ResultadosActivity.this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnHome:
                intent = new Intent(ResultadosActivity.this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                intent = new Intent(ResultadosActivity.this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(ResultadosActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", true);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void searchByCategory(String param) {
        int categoria = getIntent().getIntExtra(IDCATEGORIA, 0);
        double latitud = SessionProcessor.getLatitud(this);
        double longitud = SessionProcessor.getLongitud(this);
        param += SessionProcessor.getParametro(this);
        JSONObject parameter = new JSONObject();
        JSONArray params = new JSONArray();

        try {
            parameter.put("idcategoria", categoria + "");
            parameter.put("param", param);
            parameter.put("latitud", latitud + "");
            parameter.put("longitud", longitud + "");
            params.put(parameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonArrayRequest patchRequest = new JsonArrayRequest( Request.Method.PATCH, EmpresaService.PATCH_CATEGORIA,
            params,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        marrResultados.clear();
                        for(int i = 0; i < response.length(); i++) {
                            JSONObject obj = response.getJSONObject(i);
                            Sucursal suc = new Sucursal();
                            suc.setId(obj.getString("id"));
                            suc.setIdempresa(obj.getString("idempresa"));
                            suc.setRazonsocial(obj.getString("razonsocial"));
                            suc.setEstado(obj.getString("estado"));
                            suc.setDireccion(obj.getString("direccion"));
                            suc.setDelivery(obj.getInt("delivery"));
                            suc.setVeinticuatrohs(obj.getInt("veinticuatrohs"));
                            suc.setDirty(obj.getInt("dirty"));
                            marrResultados.add(suc);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    madpResultados.notifyDataSetChanged();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder add = new AlertDialog.Builder(ResultadosActivity.this);
                    add.setMessage(error.getMessage()).setCancelable(true);
                    AlertDialog alert = add.create();
                    alert.setTitle("Sin conexión!!");
                    alert.show();
                }
            }) {

        };
        MainApp.getPermission().addToRequestQueue(patchRequest);
    }

    private class ItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent();
            Sucursal suc = marrResultados.get(i);
            SessionProcessor.setIdEmpresa(ResultadosActivity.this, suc.getIdempresa());
            intent.setClass(ResultadosActivity.this, DetalleContactoActivity.class);
            startActivity(intent);
        }
    }

    private void searchWithParams(String param) {
        double latitud = SessionProcessor.getLatitud(this);
        double longitud = SessionProcessor.getLongitud(this);
        param += SessionProcessor.getParametro(this);
        JSONObject parameter = new JSONObject();
        JSONArray params = new JSONArray();
        try {
            parameter.put("param", param);
            parameter.put("latitud", latitud + "");
            parameter.put("longitud", longitud + "");
            params.put(parameter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonArrayRequest patchRequest = new JsonArrayRequest( Request.Method.PATCH, EmpresaService.PATCH_BUSQUEDA,
                params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            marrResultados.clear();
                            for(int i = 0; i < response.length(); i++) {
                                JSONObject obj = response.getJSONObject(i);
                                Sucursal suc = new Sucursal();
                                suc.setId(obj.getString("id"));
                                suc.setIdempresa(obj.getString("idempresa"));
                                suc.setRazonsocial(obj.getString("razonsocial"));
                                suc.setEstado(obj.getString("estado"));
                                suc.setDireccion(obj.getString("direccion"));
                                suc.setDelivery(obj.getInt("delivery"));
                                suc.setVeinticuatrohs(obj.getInt("veinticuatrohs"));
                                suc.setDirty(obj.getInt("dirty"));
                                marrResultados.add(suc);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        madpResultados.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder add = new AlertDialog.Builder(ResultadosActivity.this);
                        add.setMessage(error.getMessage()).setCancelable(true);
                        AlertDialog alert = add.create();
                        alert.setTitle("Sin conexión!!");
                        alert.show();
                    }
                }) {

        };
        MainApp.getPermission().addToRequestQueue(patchRequest);
    }
}
