package com.guiapp.guiapp;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.PaqueteAdapter;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Paquete;
import com.guiapp.guiapp.services.PaqueteService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CuentaActivity extends AppCompatActivity implements View.OnClickListener {
    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;

    PaqueteAdapter.RecyclerViewOnItemClickListener mItemOnClickListener;
    ArrayList<Paquete> marrPaquetes;
    RecyclerView mPaquetes;
    RecyclerView.Adapter madpPaquete;
    RecyclerView.LayoutManager mlmPaquete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        mDBHelper = DatabaseHelper.getInstance(this);
        mDatabase = mDBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getId().isEmpty()){
            Utils.showMessage(this, "Necesita estar registrado para acceder a esta sección.", true);
        }

        setTextosTV(R.id.tvUsuario, cuenta.getNombre());
        setTextosTV(R.id.tvCuenta, cuenta.getEmail());

//        setSwitchValue(R.id.swNotificaciones, cuenta.getNotificaciones() ==1);

        setOnClickListeners(R.id.btnVolver, false);
        setOnClickListeners(R.id.btnHome, false);
        setOnClickListeners(R.id.btnBuscar, false);
        setOnClickListeners(R.id.btnAgregar, false);
        setOnClickListeners(R.id.btnConfig, false);

        mlmPaquete = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        mItemOnClickListener = new PaqueteAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Paquete pub = marrPaquetes.get(position);
                SessionProcessor.setIdPaquete(CuentaActivity.this, pub.getId());
                Intent intent = new Intent(CuentaActivity.this, PaqueteActivity.class);
                startActivity(intent);
            }
        };
        mPaquetes = findViewById(R.id.rvPaquetes);
        mPaquetes.setHasFixedSize(true);
        mPaquetes.setLayoutManager(mlmPaquete);
        marrPaquetes = new ArrayList<>();
        madpPaquete = new PaqueteAdapter(marrPaquetes, mItemOnClickListener);
        mPaquetes.setAdapter(madpPaquete);

        getPaquetes();
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                super.onBackPressed();
                break;
            case R.id.btnHome:
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas...", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void setTextosTV(int id, String text) {
        TextView tv = findViewById(id);
        tv.setText(text);
    }

    private void setSwitchValue(int id, boolean value) {
        Switch sw = findViewById(id);
        sw.setChecked(value);
    }

    private void setOnClickListeners(int view, boolean isButton) {
        if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    private void getPaquetes() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this, "Ingresó como invitado, por lo tanto no puede acceder a configuración de cuenta.", true);
            return;
        }

        String ctmUrl = PaqueteService.getURLGetByIDCuenta(idcuenta);
        JsonArrayRequest jsonreq = new JsonArrayRequest(ctmUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrPaquetes.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Paquete paquete = new Paquete();
                                paquete.setId(obj.getInt("id"));
                                paquete.setNombre(obj.getString("nombre"));
                                paquete.setImagen(obj.getString("imagen"));
                                paquete.setDescripcion(obj.getString("descripcion"));
                                paquete.setCosto(obj.getDouble("costo"));
                                paquete.setDuracion(obj.getInt("duracion"));
                                paquete.setLogo(obj.getString("logo"));
                                paquete.setActivo(obj.getInt("activo"));
                                paquete.setPlazo(obj.getString("plazo"));

                                marrPaquetes.add(paquete);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        madpPaquete.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.showMessage(CuentaActivity.this, "Sin conexión", true);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }
}
