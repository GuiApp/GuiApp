package com.guiapp.guiapp;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.EventoAdapter;
import com.guiapp.guiapp.classes.DataInterchanger;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.EventoTipo;
import com.guiapp.guiapp.services.EventoService;
import com.guiapp.guiapp.services.EventoTipoService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CalendarioActivity extends AppCompatActivity implements View.OnClickListener, DataInterchanger {
    EventoAdapter.RecyclerViewOnItemClickListener mItemOnClickListener;
    ArrayList<Evento> marrEventos;
    RecyclerView mEventos;
    RecyclerView.Adapter madpEventos;
    RecyclerView.LayoutManager mlmEventos;

    ArrayList<EventoTipo> marrTiposEventos;

    long mSelectedIdTipo;
    String mSelectedFeccha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendario);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        mSelectedIdTipo = -1;
        mSelectedFeccha="-1";

        setOnClickListeners(R.id.btnVolver, false, false);
        setOnClickListeners(R.id.tvFecha, false,true);
        setOnClickListeners(R.id.tvTipoEvento, false, true);
        setOnClickListeners(R.id.btnBuscarEvento, true, false);
        setOnClickListeners(R.id.btnHome, false, false);
        setOnClickListeners(R.id.btnBuscar, false, false);
        setOnClickListeners(R.id.btnAgregar, false, false);
        setOnClickListeners(R.id.btnConfig, false, false);

        mlmEventos = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        mItemOnClickListener = new EventoAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Evento evento = marrEventos.get(position);
                SessionProcessor.setIdEvento(CalendarioActivity.this, evento.getId());
                Intent intent = new Intent(CalendarioActivity.this, EventoActivity.class);
                startActivity(intent);
            }
        };
        marrTiposEventos = new ArrayList<>();

        mEventos = findViewById(R.id.rvEventos);
        mEventos.setHasFixedSize(true);
        mEventos.setLayoutManager(mlmEventos);
        marrEventos = new ArrayList<>();
        madpEventos = new EventoAdapter(marrEventos, mItemOnClickListener);
        mEventos.setAdapter(madpEventos);

        getTipoEvento();
    }

    private void getEventos() {
        marrEventos.clear();
        madpEventos.notifyDataSetChanged();
        String ctmUrl = EventoService.getURLGetFiltered(mSelectedIdTipo, mSelectedFeccha);
        JsonArrayRequest jsonreq = new JsonArrayRequest(ctmUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrEventos.clear();
                        if(response.length() == 0) {
                            Utils.showMessage(CalendarioActivity.this, "No hay eventos progamados del tipo seleccionado en la fecha indicada", false);
                            return;
                        }
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Evento evento = new Evento();
                                evento.setId(obj.getString("id"));
                                evento.setNombre(obj.getString("nombre"));
                                evento.setDescripcion(obj.getString("descripcion"));
                                evento.setImagen(obj.getString("imagen"));
                                evento.setIdtipo(obj.getLong("idtipo"));
                                evento.setTipo(obj.getString("tipo"));
                                evento.setUbicacion(obj.getString("ubicacion"));
                                evento.setFecinicio(obj.getString("fecinicio"));
                                evento.setFecfin(obj.getString("fecfin"));
                                evento.setIdempresa(obj.getString("idempresa"));
                                evento.setRazonsocial(obj.getString("razonsocial"));
                                evento.setHorainicio(obj.getString("horainicio"));
                                evento.setHorafin(obj.getString("horafin"));

                                marrEventos.add(evento);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        madpEventos.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.showMessage(CalendarioActivity.this, "Sin conexión", true);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnVolver:
                onBackPressed();
                break;
            case R.id.tvTipoEvento:
                selectTipo();
                break;
            case R.id.tvFecha:
                selectFecha();
                break;
            case R.id.btnBuscarEvento:
                getEventos();
                break;
            case R.id.btnHome:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(v);
                openContextMenu(v);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void selectTipo(){
        FragmentManager manager = getFragmentManager();
        CalendarListFragment dialog = new CalendarListFragment();
        dialog.setDataInterchanger(this);
        dialog.show(manager, "cappo");
    }

    private void selectFecha() {
        CalendarDateFragment newFragment = new CalendarDateFragment();
        newFragment.setDataInterchanger(this);
        newFragment.show(getSupportFragmentManager(), "Fecha de los Eventos");
    }

    private void getTipoEvento() {
        String customUrl = EventoTipoService.GET_LISTA;
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrTiposEventos.clear();
                        EventoTipo tipo = new EventoTipo();
                        tipo.setId(-1);
                        tipo.setTipo("Tipo de Evento");
                        marrTiposEventos.add(tipo);

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                tipo = new EventoTipo();
                                tipo.setId(obj.getInt("id"));
                                tipo.setTipo(obj.getString("tipo"));
                                marrTiposEventos.add(tipo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CalendarioActivity.this, "Sin conexión!!", Toast.LENGTH_LONG).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    @Override
    public void setStringData(String value) {
        this.mSelectedFeccha = value;

        String[] s = value.split("-");
        int day = Integer.parseInt(s[2]);
        int month = Integer.parseInt(s[1]);
        int year = Integer.parseInt(s[0]);
        String d = String.format("%02d-%02d-%04d", day, month, year);

        setTextos(R.id.tvFecha, d);
    }

    @Override
    public String getStringData() {
        return this.mSelectedFeccha;
    }

    @Override
    public void setIntData(int value) {}

    @Override
    public void setLongData(Long value) {
        mSelectedIdTipo = value;
        String tipo = "Seleccionar Tipo de Evento";
        for(int i = 0; i < marrTiposEventos.size(); i++) {
            if (marrTiposEventos.get(i).getId() == mSelectedIdTipo) {
                tipo = marrTiposEventos.get(i).getTipo();
            }
        }
        setTextos(R.id.tvTipoEvento, tipo);
    }

    @Override
    public long getLongData() {
        return mSelectedIdTipo;
    }

    @Override
    public void  onChangeData() {}

    private void setTextos(int id, String text) {
        TextView et = findViewById(id);
        et.setText(text);
    }

    private void setOnClickListeners(int view, boolean isButton, boolean isTextView) {
        if(isTextView){
            TextView txt = findViewById(view);
            txt.setOnClickListener(this);
        } else if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton imb = findViewById(view);
            imb.setOnClickListener(this);
        }
    }
}
