package com.guiapp.guiapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.adapters.EmpresasSpinAdapter;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Empresa;
import com.guiapp.guiapp.entities.Funcion;
import com.guiapp.guiapp.entities.Publicacion;
import com.guiapp.guiapp.enums.Modulos;
import com.guiapp.guiapp.services.EmpresaService;
import com.guiapp.guiapp.services.FuncionService;
import com.guiapp.guiapp.services.PublicacionService;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EdicionPublicacionActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PICK_IMAGE_REQUEST= 99;

    private long mLastClickTime = 0;
    EmpresasSpinAdapter madpEmpresas;
    Publicacion mPublicacion;
    EditText mTitulo, mDescripcion;
    Spinner mEmpresa;
    String mImageLocalUrl;
    Bitmap mBitmap;
    boolean mPropietario;
    Boolean mReloadData;
    Boolean mFromChoosePhoto;

    ImageView mImagen;
    TextView mTag;
    String mLogoPath;

    boolean mLimiAlcanzado;

    Button mbtnGuardar, mbtnCancelar, mbtnVerPublicaciones;

    ArrayList<Empresa> marrEmpresas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edicion_publicacion);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        if(Build.VERSION.SDK_INT >= 23) {
            if(!checkPermission()) {
                requestPermission();
            }
        }

        mReloadData = true;
        mFromChoosePhoto = false;
        mLogoPath = "";
    }

    @Override
    protected void onResume(){
        super.onResume();

        boolean invitado = false;
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            invitado = true;
            String idpublicacion = SessionProcessor.getIdPublicacion(this);
            if(idpublicacion.isEmpty()) {
                showMessage("Con cuenta invitado no puede realizar publicaciones. Por favor regístrese. Cierre la app para volver a intentarlo.", true);
                return;
            }
        }

        mImagen = findViewById(R.id.ivImagen);
        mImagen.setOnClickListener(this);
        mTag = findViewById(R.id.tvTag);
        mTag .setOnClickListener(this);

        if(mFromChoosePhoto) {
            mFromChoosePhoto = false;
            return;
        }

        if(!mReloadData && madpEmpresas != null)
            return;

        mPropietario = false;

        setHintText(R.id.etTitulo, "Ej.: Imperdible promoción!");
        setHintText(R.id.etDescripcion, "Máximo 120 caracteres");

        mTitulo = findViewById(R.id.etTitulo);
        mDescripcion = findViewById(R.id.etDescripcion);
        mEmpresa = findViewById(R.id.spEmpresa);
        TextView tvTag = findViewById(R.id.tvTag);
        tvTag.setOnClickListener(this);
        mbtnGuardar = findViewById(R.id.btnGuardar);
        mbtnGuardar.setOnClickListener(this);
        mbtnCancelar = findViewById(R.id.btnCancelar);
        mbtnCancelar.setOnClickListener(this);
        mbtnVerPublicaciones = findViewById(R.id.btnVerPublicaciones);
        mbtnVerPublicaciones.setOnClickListener(this);

        int visibility = View.VISIBLE;
        boolean enabled = true;
        if(invitado) {
            visibility = View.GONE;
            enabled = false;
        } else {
            verifyLimite();
        }

        mbtnGuardar.setVisibility(visibility);
        mbtnCancelar.setVisibility(visibility);
        mbtnVerPublicaciones.setVisibility(visibility);

        mTitulo.setEnabled(enabled);
        mDescripcion.setEnabled(enabled);
        mEmpresa.setEnabled(enabled);

        setOnClickListenersIM(R.id.btnVolver);
        setOnClickListenersIM(R.id.btnHome);
        setOnClickListenersIM(R.id.btnBuscar);
        setOnClickListenersIM(R.id.btnConfig);

        marrEmpresas = new ArrayList<>();
        madpEmpresas = new EmpresasSpinAdapter(this, R.layout.spinner_item, marrEmpresas);
        madpEmpresas.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mEmpresa.setAdapter(madpEmpresas);

        getPublicacionData();

        mImageLocalUrl = "";
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        showKeyboard(R.id.etTitulo);
    }

    private void verifyLimite() {
        String idpublicacion = SessionProcessor.getIdPublicacion(this);
        if(!idpublicacion.isEmpty())
            return;

        Funcion funcion = new Funcion();
        funcion.loadPermisos(this);
        mLimiAlcanzado = funcion.getLimAlcanzado(Modulos.publicacion);
        if(mLimiAlcanzado) {
            showMessage("No puede crear nuevas publicaciones", true);
        }
    }

    private void checkProperty() {
        if(marrEmpresas.size() == 0)
            return;

        if(mPropietario) {
            mTitulo.setEnabled(true);
            mDescripcion.setEnabled(true);
            mImagen.setEnabled(true);
            mEmpresa.setEnabled(true);
            mbtnGuardar.setVisibility(View.VISIBLE);
            mbtnCancelar.setVisibility(View.VISIBLE);
            mbtnVerPublicaciones.setVisibility(View.VISIBLE);
        } else {
            mTitulo.setEnabled(false);
            mDescripcion.setEnabled(false);
            mImagen.setEnabled(false);
            mEmpresa.setEnabled(false);
            mbtnGuardar.setVisibility(View.GONE);
            mbtnCancelar.setVisibility(View.GONE);
            mbtnVerPublicaciones.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mReloadData = false;
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            assert resultUri != null;
            File f = new File(resultUri.getPath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = true;
            mBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
            mLogoPath = resultUri.getPath();
            mFromChoosePhoto = true;
            Picasso.get().load(f).fit().into(mImagen);
            hideSeleccionarImagenTV();
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            assert cropError != null;
            Utils.showMessage(this,cropError.getMessage(), false);
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            String tempname = 'p' + UUID.randomUUID().toString();
            UCrop.of(filePath, Uri.fromFile(new File(getCacheDir(), tempname)))
                    .withAspectRatio(788, 1400)
                    .withMaxResultSize(788, 1400)
                    .start(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000){
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnCancelar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(EdicionPublicacionActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
                SessionProcessor.setIdPublicacion(this, "");
                break;
            case R.id.ivImagen:
                mImagen.setOnClickListener(null);
                mTag.setOnClickListener(null);
                showFileChooser();
                break;
            case R.id.tvTag:
                mImagen.setOnClickListener(null);
                mTag.setOnClickListener(null);
                showFileChooser();
                break;
            case R.id.btnVerPublicaciones:
                SessionProcessor.setIdEmpresa(this, mPublicacion.getIdempresa());
                intent = new Intent(EdicionPublicacionActivity.this, PublicacionesActivity.class);
                startActivity(intent);
                SessionProcessor.setIdPublicacion(this, "");
                break;
            case R.id.btnVolver:
                SessionProcessor.setIdPublicacion(this, "");
                super.onBackPressed();
                break;
            case R.id.btnHome:
                intent = new Intent(EdicionPublicacionActivity.this, DashboardActivity.class);
                startActivity(intent);
                SessionProcessor.setIdPublicacion(this, "");
                break;
            case R.id.btnBuscar:
                intent = new Intent(EdicionPublicacionActivity.this, BusquedaActivity.class);
                startActivity(intent);
                SessionProcessor.setIdPublicacion(this, "");
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(EdicionPublicacionActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
                SessionProcessor.setIdPublicacion(this, "");
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                SessionProcessor.setIdPublicacion(this, "");
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(EdicionPublicacionActivity.this, "Permiso otorgado correctamente! ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(EdicionPublicacionActivity.this, "Permiso denegado :(", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void showKeyboard(int view) {
        EditText yourEditText= findViewById(view);
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.showSoftInput(yourEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        SessionProcessor.setIdEmpresa(this, "");
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void setOnClickListenersIM(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    private void getPublicacionData() {
        String idpublicacion = SessionProcessor.getIdPublicacion(this);
        mPublicacion = new Publicacion();
        if(idpublicacion.isEmpty()) {
            mPropietario = true;
            getEmpresas();
            return;
        }

        String customUrl = PublicacionService.getURLGetByID(idpublicacion);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mPublicacion.setId(obj.getString("id"));
                            mPublicacion.setIdempresa(obj.getString("idempresa"));
                            mPublicacion.setRazonsocial(obj.getString("razonsocial"));
                            mPublicacion.setTitulo(obj.getString("titulo"));
                            mPublicacion.setImagen(obj.getString("imagen"));
                            mPublicacion.setDescripcion(obj.getString("descripcion"));
                            mPublicacion.setDestacada(obj.getInt("destacada"));
                            mPublicacion.setFecmodificacion(obj.getString("fecmodificacion"));
                            mPublicacion.setDirty(obj.getInt("dirty"));
                            mPublicacion.setIdcuenta(obj.getString("idcuenta"));

                            if(SessionProcessor.getIdCuenta(EdicionPublicacionActivity.this).equals(mPublicacion.getIdcuenta()))
                                mPropietario = true;
                            getEmpresas();
                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder add = new AlertDialog.Builder(EdicionPublicacionActivity.this);
                        add.setMessage(error.getMessage()).setCancelable(true);
                        AlertDialog alert = add.create();
                        alert.setTitle("Sin conexión!!");
                        alert.show();
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);

        String customUrl;
        if(mPropietario) {
            customUrl = EmpresaService.getURLXCuenta(idcuenta);
        }else {
            customUrl = EmpresaService.getURLGetByID(mPublicacion.getIdempresa());
        }
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    marrEmpresas.clear();
                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Empresa empresa = new Empresa();
                            empresa.setId(obj.getString("id"));
                            empresa.setRazonsocial(obj.getString("razonsocial"));
                            empresa.setCuit(obj.getLong("cuit"));
                            empresa.setMediodepago(obj.getString("mediodepago"));
                            empresa.setEstado(obj.getString("estado"));
                            empresa.setLogo(obj.getString("logo"));
                            empresa.setFacebook(obj.getString("facebook"));
                            empresa.setTwitter(obj.getString("twitter"));
                            empresa.setInstagram(obj.getString("instagram"));
                            empresa.setPalabrasclave(obj.getString("palabrasclave"));
                            empresa.setDirty(obj.getInt("dirty"));
                            empresa.setDescripcion(obj.getString("descripcion"));
                            empresa.setIdcuenta(obj.getString("idcuenta"));
                            marrEmpresas.add(empresa);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if(marrEmpresas.size() == 0) {
                        showMessage("No posee empresas registradas. Por favor agregue una empresa.", true);
                    }

                    madpEmpresas.notifyDataSetChanged();
//                    updateInfo();
                    checkProperty();
                    int i = 0;
                    String idEmpresa;
                    if (mPublicacion.getIdempresa().length() == 0) {
                        idEmpresa = SessionProcessor.getIdEmpresa(EdicionPublicacionActivity.this);
                    } else {
                        idEmpresa = mPublicacion.getIdempresa();
                    }

                    for(Empresa emp : marrEmpresas) {
                        if(emp.getId().equals(idEmpresa)){
                            mEmpresa.setSelection(i);
                            break;
                        }
                        i++;
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(Utils.DEBUG) {
                        Utils.showMessage(EdicionPublicacionActivity.this, "Error de sincronización: " + error.getMessage(), false);
                    } else {
                        Utils.showMessage(EdicionPublicacionActivity.this, "No dispone de conexión. Por favor verifique", false);
                    }
                }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        try {
            Uri.parse(mPublicacion.getImagen());
        } catch (Exception ignored) {}

        mTitulo.setText(mPublicacion.getTitulo());
        mDescripcion.setText(mPublicacion.getDescripcion());

        loadImagen();
    }

    private void loadImagen() {
        if(!URLUtil.isValidUrl(mPublicacion.getImagen())) {
            Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.default_publicacion);
            mImagen.setImageBitmap(logo);
        } else {
//            Picasso.get().invalidate(mPublicacion.getImagen());
            Picasso.get().load(mPublicacion.getImagen()).fit().into(mImagen);
            hideSeleccionarImagenTV();
        }
    }

    private void hideSeleccionarImagenTV() {
        TextView tvTag = findViewById(R.id.tvTag);
        tvTag.setVisibility(View.INVISIBLE);
    }

    private void setHintText(int id, String s) {
        EditText edittext = findViewById(id);
        Spanned text = Html.fromHtml("<i>" + s + "</i>");
        edittext.setHint(text);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        mFromChoosePhoto = true;
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), PICK_IMAGE_REQUEST);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(EdicionPublicacionActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(EdicionPublicacionActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(EdicionPublicacionActivity.this, " Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(EdicionPublicacionActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private void guardar() {
        if (mPublicacion.getId().isEmpty())
            post();
        else
            put();
    }

    private void post() {
        mPublicacion.setTitulo(mTitulo.getText().toString());
        mPublicacion.setDestacada(1);
        mPublicacion.setImagen("");
        Empresa empresa = (Empresa) mEmpresa.getSelectedItem();
        mPublicacion.setIdempresa(empresa.getId());
        mPublicacion.setDescripcion(mDescripcion.getText().toString());
        mPublicacion.setFecmodificacion("");

        if(mLogoPath.isEmpty()) {
            showMessage("Falta imagen!!", false);
            return;
        }

        if(mTitulo.getText().toString().isEmpty()
                || mDescripcion.getText().toString().isEmpty()
                || mEmpresa.getSelectedItemPosition() < 0) {
            showMessage("Falta completar datos!!", false);
            return;
        }

        mPublicacion.setImagen(getStringImage(mBitmap));
        mPublicacion.setId(UUID.randomUUID().toString());
        HashMap<String, String> map = mPublicacion.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = PublicacionService.POST_FROMMOBILE;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showMessage("La publicación se ha guardado con éxito!!", false);
//                        postImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EdicionPublicacionActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                        getFunciones();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mPublicacion.setId("");
                        showMessage("La publicación no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EdicionPublicacionActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getFunciones() {
        DatabaseHelper dBHelper = DatabaseHelper.getInstance(this);
        SQLiteDatabase mDatabase = dBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getSync() == 1)
            return;

        String url = FuncionService.getURLGetByIDCuenta(cuenta.getId());
        JsonArrayRequest jsonreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        SessionProcessor.setFunciones(EdicionPublicacionActivity.this, response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Utils.DEBUG)
                    Toast.makeText(EdicionPublicacionActivity.this, "Error: getFunciones",Toast.LENGTH_SHORT).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void put() {
        mPublicacion.setTitulo(mTitulo.getText().toString());
        Empresa empresa = (Empresa) mEmpresa.getSelectedItem();
        mPublicacion.setIdempresa(empresa.getId());
        mPublicacion.setDescripcion(mDescripcion.getText().toString());
        mPublicacion.setFecmodificacion("");
        mPublicacion.setDirty(0);

        if(mTitulo.getText().toString().isEmpty()
                || mDescripcion.getText().toString().isEmpty()) {
            showMessage("Falta completar datos!!", false);
            return;
        }
        mPublicacion.setImagen(getStringImage(mBitmap));

        HashMap<String, String> map = mPublicacion.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = PublicacionService.PUT_FROMMOBILE; // PublicacionService.getURLPut(mPublicacion.getId());

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showMessage("La publicación se ha guardado con éxito!!", false);
//                        updateImagen();
                        if (Utils.DEBUG)
                            Toast.makeText(EdicionPublicacionActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMessage("La publicación no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(EdicionPublicacionActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void showMessage(String mensaje, Boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Guiapp");
        if(!salir) {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        } else {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    Intent intent = new Intent(EdicionPublicacionActivity.this, DashboardActivity.class);
                    startActivity(intent);
                }
            });
        }
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }

//    public void postImagen(){
//
//        if(mLogoPath.isEmpty())
//            return;
//
//        String customUrl = PublicacionService.POST_IMAGEN;
//        Map<String,String> param = new HashMap<>();
//
//        String images = getStringImage(mBitmap);
//        param.put("id", mPublicacion.getId());
//        param.put("image",images);
//        JSONObject jobject = new JSONObject(param);
//
//        JsonObjectRequest jsonreq = new JsonObjectRequest(
//                Request.Method.POST,
//                customUrl,
//                jobject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        //showMessage("La imagen se ha guardado con éxito!!");
//                        if(Utils.DEBUG)
//                            Toast.makeText(EdicionPublicacionActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        showMessage("La imagen no se ha podido guardar!! \n Por favor reintente.", false);
//                    }
//                }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8" + getParamsEncoding();
//            }
//        };
//
//        MainApp.getPermission().addToRequestQueue(jsonreq);
//    }
//
//    public void updateImagen(){
//
//        if(mLogoPath.isEmpty())
//            return;
//
//        String customUrl = PublicacionService.PUT_IMAGEN;
//        Map<String,String> param = new HashMap<>();
//
//        String images = getStringImage(mBitmap);
//        param.put("id", mPublicacion.getId());
//        param.put("image",images);
//        JSONObject jobject = new JSONObject(param);
//
//        JsonObjectRequest jsonreq = new JsonObjectRequest(
//                Request.Method.PUT,
//                customUrl,
//                jobject,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        //showMessage("La imagen se ha guardado con éxito!!");
//                        if(Utils.DEBUG)
//                            Toast.makeText(EdicionPublicacionActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        showMessage("La imagen no se ha podido guardar!! \n Por favor reintente.", false);
//                    }
//                }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8" + getParamsEncoding();
//            }
//        };
//
//        MainApp.getPermission().addToRequestQueue(jsonreq);
//    }

    public String getStringImage(Bitmap bitmap){
        Log.i("MyHitesh",""+bitmap);
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,40, baos);
        byte [] b=baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp.replace("\n", "");
    }
}
