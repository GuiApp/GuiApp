package com.guiapp.guiapp;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.adapters.CarouselAdapter;
import com.guiapp.guiapp.adapters.CarouselCalendarioAdapter;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.Publicacion;
import com.guiapp.guiapp.services.CuentaService;
import com.guiapp.guiapp.services.EventoService;
import com.guiapp.guiapp.services.FuncionService;
import com.guiapp.guiapp.services.PublicacionService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener {
    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;

    private RecyclerView.LayoutManager mlmDestacados;
    private RecyclerView.LayoutManager mlmRecientes;
    private RecyclerView.LayoutManager mlmCalendario;

    private RecyclerView.Adapter madpDestacados;
    private RecyclerView.Adapter madpRecientes;
    private RecyclerView.Adapter madpCalendario;

    private ArrayList<Publicacion> marrDestacados;
    private ArrayList<Publicacion> marrRecientes;
    private ArrayList<Evento> marrCalendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        mDBHelper = DatabaseHelper.getInstance(this);
        mDatabase = mDBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        syncCuenta();

        TextView tvCcuenta = findViewById(R.id.tvCuenta);
        if(cuenta.getId().isEmpty()) {
            tvCcuenta.setText("Regístrate grátis.");
            tvCcuenta.setOnClickListener(this);
        } else {
            tvCcuenta.setText(cuenta.getNombre());
            SessionProcessor.setIdCuenta(this, cuenta.getId());
        }

        Typeface tf = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        TextView tvLeftDestacados = findViewById(R.id.tvLeftDestacados);
        TextView tvLeftRecientes = findViewById(R.id.tvLeftRecientes);
        TextView tvLeftCalendario = findViewById(R.id.tvLeftCalendario);
        tvLeftDestacados.setTypeface(tf);
        tvLeftRecientes.setTypeface(tf);
        tvLeftCalendario.setTypeface(tf);

        tf = Typeface.createFromAsset(getAssets(), "dosis_regular.ttf");
        TextView tvTagDestacados = findViewById(R.id.tvDestacados);
        TextView tvTagRecientes = findViewById(R.id.tvRecientes);
        TextView tvTagCalendario = findViewById(R.id.tvCalendario);
        tvTagDestacados.setTypeface(tf);
        tvTagRecientes.setTypeface(tf);
        tvTagCalendario.setTypeface(tf);

        int displayHeight = Utils.getScreenSize(this).y;

        createLayoutManager();
        CarouselAdapter.RecyclerViewOnItemClickListener rVOnItemClickListenerPubDestacados = new CarouselAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Publicacion pub = marrDestacados.get(position);
                SessionProcessor.setIdPublicacion(DashboardActivity.this, pub.getId());
                Intent intent = new Intent(DashboardActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
            }
        };
        RecyclerView rvDestacados = findViewById(R.id.rvDestacados);
        rvDestacados.setHasFixedSize(true);
        rvDestacados.setLayoutManager(mlmDestacados);
        marrDestacados = new ArrayList<>();
        int heigth = (int)(displayHeight / 3.5); // 2.7);
        madpDestacados = new CarouselAdapter(marrDestacados, heigth, rVOnItemClickListenerPubDestacados);
        rvDestacados.setAdapter(madpDestacados);
        setDestacadosData();

        CarouselAdapter.RecyclerViewOnItemClickListener rVOnItemClickListenerPubRecientes = new CarouselAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Publicacion pub = marrRecientes.get(position);
                SessionProcessor.setIdPublicacion(DashboardActivity.this, pub.getId());
                Intent intent = new Intent(DashboardActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
            }
        };
        RecyclerView rvRecientes = findViewById(R.id.rvRecientes);
        rvRecientes.setHasFixedSize(true);
        rvRecientes.setLayoutManager(mlmRecientes);
        marrRecientes = new ArrayList<>();
        heigth = (int)(displayHeight / 4.5); // 3.7);
        madpRecientes = new CarouselAdapter(marrRecientes, heigth, rVOnItemClickListenerPubRecientes);
        rvRecientes.setAdapter(madpRecientes);
        setRecientesData();

        CarouselCalendarioAdapter.RecyclerViewOnItemClickListener rVOnItemClickListenerCalendario = new CarouselCalendarioAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                Evento evento = marrCalendario.get(position);
                SessionProcessor.setIdEvento(DashboardActivity.this, evento.getId());
                Intent intent = new Intent(DashboardActivity.this, EventoActivity.class);
                startActivity(intent);
            }
        };
        RecyclerView rvCalendario = findViewById(R.id.rvCalendario);
        rvCalendario.setHasFixedSize(true);
        rvCalendario.setLayoutManager(mlmCalendario);
        marrCalendario = new ArrayList<>();
        madpCalendario = new CarouselCalendarioAdapter(marrCalendario, rVOnItemClickListenerCalendario);
        rvCalendario.setAdapter(madpCalendario);
        getEventosData();

//        ImageButton ibHome = findViewById(R.id.btnHome);
        ImageButton ibBuscar = findViewById(R.id.btnBuscar);
        ImageButton ibAgregar = findViewById(R.id.btnAgregar);
        ImageButton ibConfiguracion = findViewById(R.id.btnConfig);

        ibBuscar.setOnClickListener(this);
        ibAgregar.setOnClickListener(this);
        ibConfiguracion.setOnClickListener(this);

        getFunciones();
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.btnConfig) {
            registerForContextMenu(view);
            openContextMenu(view);
            return;
        }

        Intent intent = null;
        if(view.getId() == R.id.tvCuenta) {
            String idcuenta = SessionProcessor.getIdCuenta(this);
            if(idcuenta.equals(Cuenta.INVITADO))
                intent = new Intent(DashboardActivity.this, LoginActivity.class);
        }

        if(view.getId() == R.id.btnBuscar) {
            intent = new Intent(DashboardActivity.this, BusquedaActivity.class);
        }

        if(view.getId() == R.id.btnAgregar) {
            SessionProcessor.setIdPublicacion(this, "");
            intent = new Intent(DashboardActivity.this, EdicionPublicacionActivity.class);
        }
        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    //*********************************
    //***   Sinconización de datos  ***
    //*********************************
    public void syncCuenta() {
        mDBHelper = DatabaseHelper.getInstance(this);
        mDatabase = mDBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getSync() == 1)
            return;

        HashMap<String, String> map = cuenta.getHashMap();

        // Crear nuevo objeto Json basado en el mapa
        JSONObject jobject = new JSONObject(map);
        // Depurando objeto Json...
        Log.d(this.getLocalClassName(), jobject.toString());

        // Actualizar datos en el servidor
        JsonObjectRequest jsonreq =  new JsonObjectRequest(
                Request.Method.POST,
                CuentaService.API_CALL,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Procesar la respuesta del servidor
                        if(Utils.DEBUG)
                            Toast.makeText(DashboardActivity.this, "Cuenta sincronizada",Toast.LENGTH_SHORT).show();

                        mDBHelper = DatabaseHelper.getInstance(DashboardActivity.this);
                        mDatabase = mDBHelper.getWritableDatabase();
                        Cuenta cuenta = new Cuenta();
                        cuenta.fill(mDatabase);
                        cuenta.setSync(1);
                        cuenta.save(mDatabase);
                        mDatabase.close();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(Utils.DEBUG)
                            Toast.makeText(DashboardActivity.this, "Error: syncCuenta",Toast.LENGTH_SHORT).show();
                    }
                }
            )
            {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8" + getParamsEncoding();
                }
            };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getFunciones() {
        mDBHelper = DatabaseHelper.getInstance(this);
        mDatabase = mDBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getSync() == 1)
            return;

        String url = FuncionService.getURLGetByIDCuenta(cuenta.getId());
        JsonArrayRequest jsonreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        SessionProcessor.setFunciones(DashboardActivity.this, response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Utils.DEBUG)
                    Toast.makeText(DashboardActivity.this, "Error: getFunciones",Toast.LENGTH_SHORT).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getEventosData() {
        JsonArrayRequest jsonreq = new JsonArrayRequest(EventoService.GET_LISTADO,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Evento ev = new Evento();
                                ev.setId(obj.getString("id"));
                                ev.setNombre(obj.getString("nombre"));
                                ev.setDescripcion(obj.getString("descripcion"));
                                ev.setImagen(obj.getString("imagen"));
                                ev.setIdtipo(obj.getInt("idtipo"));
                                ev.setTipo(obj.getString("tipo"));
                                ev.setUbicacion(obj.getString("ubicacion"));
                                ev.setFecinicio(obj.getString("fecinicio"));
                                ev.setFecfin(obj.getString("fecfin"));
                                ev.setIdempresa(obj.getString("idempresa"));
                                ev.setHorainicio(obj.getString("horainicio"));
                                ev.setHorafin(obj.getString("horafin"));
                                marrCalendario.add(ev);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        madpCalendario.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("No dispone de conexión. Por favor verifique", true);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void setRecientesData() {
        JsonArrayRequest jsonreq = new JsonArrayRequest(PublicacionService.GET_PUBLICACIONES,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Publicacion pub = new Publicacion();
                                pub.setId(obj.getString("id"));
                                pub.setIdempresa(obj.getString("idempresa"));
                                pub.setTitulo(obj.getString("titulo"));
                                pub.setImagen(obj.getString("imagen"));
                                pub.setDescripcion(obj.getString("descripcion"));
                                pub.setDestacada(obj.getInt("destacada"));
                                pub.setFecmodificacion(obj.getString("fecmodificacion"));
                                pub.setDirty(obj.getInt("dirty"));
                                marrRecientes.add(pub);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        madpRecientes.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("No dispone de conexión. Por favor verifique", true);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void setDestacadosData() {
        JsonArrayRequest jsonreq = new JsonArrayRequest(PublicacionService.GET_DESTACADAS,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Publicacion pub = new Publicacion();
                                pub.setId(obj.getString("id"));
                                pub.setIdempresa(obj.getString("idempresa"));
                                pub.setTitulo(obj.getString("titulo"));
                                pub.setImagen(obj.getString("imagen"));
                                pub.setDescripcion(obj.getString("descripcion"));
                                pub.setDestacada(obj.getInt("destacada"));
                                pub.setFecmodificacion(obj.getString("fecmodificacion"));
                                pub.setDirty(obj.getInt("dirty"));
                                marrDestacados.add(pub);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        madpDestacados.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showMessage("No dispone de conexión. Por favor verifique", true);
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void createLayoutManager() {
        mlmDestacados = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        mlmRecientes = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };

        mlmCalendario = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            showMessage("Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            showMessage("Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        SessionProcessor.setIdEmpresa(this, "");
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void showMessage(String mensaje, final boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Guiapp");
        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(salir){
                    DashboardActivity.this.finish();
                }
            }
        });
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }
}
