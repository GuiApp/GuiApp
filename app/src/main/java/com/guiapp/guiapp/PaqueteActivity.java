package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.maps.model.LatLng;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Paquete;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.services.PaqueteService;
import com.guiapp.guiapp.services.SucursalService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaqueteActivity extends AppCompatActivity implements View.OnClickListener {

    Paquete mPaquete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paquete);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SessionProcessor.getIdPaquete(this) == -1) {
            Utils.showMessage(this, "No se ha seleccionado un paquete.", true);
        } else {
            getPaqueteData();
        }

        setOnClickListeners(R.id.btnVolver, false);
        setOnClickListeners(R.id.btnObtener, true);
        setOnClickListeners(R.id.btnHome, false);
        setOnClickListeners(R.id.btnBuscar, false);
        setOnClickListeners(R.id.btnAgregar, false);
        setOnClickListeners(R.id.btnConfig, false);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                super.onBackPressed();
                break;
            case R.id.btnObtener:
                obtener();
                break;
            case R.id.btnHome:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPaquete(this, -1);
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    private void obtener() {
        Intent  intent = new Intent(this, PagoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void setOnClickListeners(int view, boolean isButton) {
        if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    private void getPaqueteData() {
        int idPaquete = SessionProcessor.getIdPaquete(this);

        String customUrl = PaqueteService.getURLGetByID(idPaquete);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mPaquete = new Paquete();
                            mPaquete.setId(obj.getInt("id"));
                            mPaquete.setImagen(obj.getString("imagen"));
                            mPaquete.setNombre(obj.getString("nombre"));
                            mPaquete.setDescripcion(obj.getString("descripcion"));
                            mPaquete.setCosto(obj.getDouble("costo"));
                            mPaquete.setDuracion(obj.getInt("duracion"));
                            mPaquete.setLogo(obj.getString("logo"));
                            mPaquete.setPlazo(obj.getString("plazo"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(PaqueteActivity.this, error.getMessage(), false);
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        setTextosTV(R.id.tvPaquete, mPaquete.getNombre());
        setTextosTV(R.id.tvDetalle, mPaquete.getDescripcion());
        setTextosTV(R.id.tvPrecio, "$ " + mPaquete.getCosto() + "");
        setTextosTV(R.id.tvSubdetalle, mPaquete.getDuracion() + " días");

        if(mPaquete.getPlazo().length() > 0) {
            setTextosTV(R.id.tvPlazo, mPaquete.getPlazo());
            Button btn = findViewById(R.id.btnObtener);
            btn.setVisibility(View.GONE);
        } else {
            TextView tv = findViewById(R.id.tvPlazo);
            tv.setVisibility(View.GONE);
        }

        ImageView imageView = findViewById(R.id.ivImagen);
        Picasso.get().load(mPaquete.getImagen()).into(imageView);
    }

    private void setTextosTV(int id, String text) {
        TextView tv = findViewById(id);
        tv.setText(text);
    }
}
