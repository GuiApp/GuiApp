package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaCas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.adapters.GaleriaAdapter;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Imagen;
import com.guiapp.guiapp.services.ImagenService;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GaleriaActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PICK_IMAGE_REQUEST= 99;

    ImageView mImagen;
    private RecyclerView.Adapter madpGaleria;
    private ArrayList<Imagen> marrGaleria;
    Bitmap mBitmap;
    Boolean mReloadData;
    Boolean mFromChoosePhoto;
    String mLogoPath;

    Imagen mImagenActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        RecyclerView.LayoutManager lmGaleria = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        GaleriaAdapter.RecyclerViewOnItemClickListener rVOnItemClickListenerGaleria = new GaleriaAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                mImagenActual = marrGaleria.get(position);

                if(!URLUtil.isValidUrl(mImagenActual.getUrl())) {
                    Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.default_publicacion);
                    mImagen.setImageBitmap(logo);
                } else {
                    Picasso.get().invalidate(mImagenActual.getUrl());
                    Picasso.get().load(mImagenActual.getUrl()).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE).placeholder(R.drawable.default_evento).error(R.drawable.default_evento).into(mImagen);
                }
            }
        };

        RecyclerView galeria = findViewById(R.id.rvGaleria);
        galeria.setHasFixedSize(true);
        galeria.setLayoutManager(lmGaleria);
        marrGaleria = new ArrayList<>();
        madpGaleria = new GaleriaAdapter(marrGaleria, rVOnItemClickListenerGaleria);
        galeria.setAdapter(madpGaleria);

        mReloadData = true;
        mFromChoosePhoto = false;
        mImagenActual = new Imagen();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mFromChoosePhoto) {
            mFromChoosePhoto = false;
            return;
        }

        if(!mReloadData)
            return;

        mImagen = findViewById(R.id.ivImagen);
        setOnClickListeners(R.id.btnVolver, false, false);
        setOnClickListeners(R.id.ivImagen, false, true);
        setOnClickListeners(R.id.btnNueva, true, false);
        setOnClickListeners(R.id.btnHome, false, false);
        setOnClickListeners(R.id.btnBuscar, false, false);
        setOnClickListeners(R.id.btnAgregar, false, false);
        setOnClickListeners(R.id.btnConfig, false, false);

        getImagenes();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnVolver:
                onBackPressed();
                break;
            case R.id.ivImagen:
                showFileChooser();
                break;
            case R.id.btnNueva:
                nueva();
                break;
            case R.id.btnHome:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(v);
                openContextMenu(v);
                break;
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mReloadData = false;
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            assert resultUri != null;
            File f = new File(resultUri.getPath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inJustDecodeBounds = true;
            mBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
            mLogoPath = resultUri.getPath();
            mFromChoosePhoto = true;
            Picasso.get().load(f).fit().into(mImagen);

            if(mImagenActual.getId().isEmpty()){
                mImagenActual.setIdempresa(SessionProcessor.getIdEmpresa(this));
                postImagen();
            } else {
                putImagen();
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            assert cropError != null;
            Utils.showMessage(this,cropError.getMessage(), false);
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            String tempname = 'p' + UUID.randomUUID().toString();
            UCrop.of(filePath, Uri.fromFile(new File(getCacheDir(), tempname)))
                    .withAspectRatio(16, 9)
                    .withMaxResultSize(271, 393)
                    .start(this);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void getImagenes() {
        String customUrl = ImagenService.getURLGetByIDEmpresa(SessionProcessor.getIdEmpresa(this));
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(marrGaleria.size() > 0)
                            marrGaleria.clear();

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Imagen imag = new Imagen();
                                imag.setId(obj.getString("id"));
                                imag.setIdempresa(obj.getString("idempresa"));
                                imag.setUrl(obj.getString("url"));

                                marrGaleria.add(imag);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        enableDisableButton(marrGaleria.size() < 6);

                        madpGaleria.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(GaleriaActivity.this);
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Sin conexión!!");
                alert.show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        mFromChoosePhoto = true;
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), PICK_IMAGE_REQUEST);
    }

    private void enableDisableButton(boolean enable) {
        Button nuevo = findViewById(R.id.btnNueva);
        nuevo.setEnabled(enable);
    }

    private void nueva() {
         mImagenActual = new Imagen();
        showFileChooser();
    }

    private void setOnClickListeners(int view, boolean isButton, boolean isImageView) {
        if(isImageView) {
            ImageView imv = findViewById(view);
            imv.setOnClickListener(this);
        } else if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    public void postImagen(){

        if(mLogoPath.isEmpty())
            return;

        String customUrl = ImagenService.POST;
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mImagenActual.getId());
        param.put("idempresa", mImagenActual.getIdempresa());
        param.put("url", mImagenActual.getUrl());
        param.put("imagen",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        getImagenes();
                        if(Utils.DEBUG)
                            Toast.makeText(GaleriaActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(GaleriaActivity.this, "La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                        showDefaulPlaceholder();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    public void putImagen(){

        if(mLogoPath.isEmpty())
            return;

        String customUrl = ImagenService.getURLPut(mImagenActual.getId());
        Map<String,String> param = new HashMap<>();

        String images = getStringImage(mBitmap);
        param.put("id", mImagenActual.getId());
        param.put("idempresa", mImagenActual.getIdempresa());
        param.put("url", mImagenActual.getUrl());
        param.put("imagen",images);
        JSONObject jobject = new JSONObject(param);

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //showMessage("La imagen se ha guardado con éxito!!");
                        getImagenes();
                        if(Utils.DEBUG)
                            Toast.makeText(GaleriaActivity.this, "La imagen se ha guardado con éxito!!", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(GaleriaActivity.this, "La imagen no se ha podido guardar!! \n Por favor reintente.", false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    protected void showDefaulPlaceholder() {
        Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.default_galeria);
        mImagen.setImageBitmap(logo);
    }

    public String getStringImage(Bitmap bitmap){
        Log.i("MyHitesh",""+bitmap);
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,40, baos);
        byte [] b=baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp.replace("\n", "");
    }
}
