package com.guiapp.guiapp;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.EventoTipoAdapter;
import com.guiapp.guiapp.classes.DataInterchanger;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.entities.EventoTipo;
import com.guiapp.guiapp.services.EventoTipoService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.widget.AbsListView.CHOICE_MODE_SINGLE;

public class CalendarListFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    ListView mlstTipos;
    private ArrayList<EventoTipo> marrTiposEventos;
    private EventoTipoAdapter madpTipoEvento;
    private DataInterchanger mDataInterchanger;

    public void setDataInterchanger(DataInterchanger value) {
        this.mDataInterchanger = value;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tipoevent_dialog_fragment, null, false);
        mlstTipos = view.findViewById(R.id.list);
        mlstTipos.setSelector(android.R.color.holo_blue_light);
        mlstTipos.setChoiceMode(CHOICE_MODE_SINGLE);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        marrTiposEventos = new ArrayList<>();
        madpTipoEvento = new EventoTipoAdapter(getActivity(),
                R.layout.item_tipoevento, marrTiposEventos);

        mlstTipos.setAdapter(madpTipoEvento);
        mlstTipos.setOnItemClickListener(this);
        getTipoEvento(this.mDataInterchanger.getLongData());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismiss();
        long idSel = marrTiposEventos.get(position).getId();
        this.mDataInterchanger.setLongData(idSel);
    }

    private void getTipoEvento(final long idToSelect) {
        String customUrl = EventoTipoService.GET_LISTA;
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int posSelected = 0;
                        marrTiposEventos.clear();
                        EventoTipo tipo = new EventoTipo();
                        tipo.setId(-1);
                        tipo.setTipo("Todos los Tipo de Evento");
                        marrTiposEventos.add(tipo);
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                tipo = new EventoTipo();
                                tipo.setId(obj.getInt("id"));
                                tipo.setTipo(obj.getString("tipo"));
                                marrTiposEventos.add(tipo);
                                if(tipo.getId() == idToSelect)
                                    posSelected = i;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if(marrTiposEventos.size() > 0) {
                            mlstTipos.setSelection(posSelected + 1);
                        }
                        madpTipoEvento.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Sin conexión!!", Toast.LENGTH_LONG).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }
}
