package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.GaleriaAdapter;
import com.guiapp.guiapp.adapters.SucursalAdapter;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Empresa;
import com.guiapp.guiapp.entities.Imagen;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.services.EmpresaService;
import com.guiapp.guiapp.services.ImagenService;
import com.guiapp.guiapp.services.SucursalService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetalleContactoActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView.Adapter madpGaleria;
    private ArrayList<Imagen> marrGaleria;

    private SucursalAdapter madpSucursales;
    private ArrayList<Sucursal> marrSucursales;

    private Empresa mEmpresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_contacto);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        mEmpresa = new Empresa();

        RecyclerView.LayoutManager lmGaleria = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        GaleriaAdapter.RecyclerViewOnItemClickListener rVOnItemClickListenerGaleria = new GaleriaAdapter.RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
            }
        };
        RecyclerView galeria = findViewById(R.id.rvGaleria);
        galeria.setHasFixedSize(true);
        galeria.setLayoutManager(lmGaleria);
        marrGaleria = new ArrayList<>();
        madpGaleria = new GaleriaAdapter(marrGaleria, rVOnItemClickListenerGaleria);
        galeria.setAdapter(madpGaleria);

        marrSucursales = new ArrayList<>();
        madpSucursales = new SucursalAdapter(this, R.id.lvSucursales, marrSucursales);
        ListView sucursales = findViewById(R.id.lvSucursales);
        sucursales.setAdapter(madpSucursales);
        sucursales.setOnItemClickListener(new ItemClickListener());

        getSucursales();
        getEmpresaData();
        getImagenes();

        setOnClickListeners(R.id.btnVolver);
        setOnClickListeners(R.id.btnHome);
        setOnClickListeners(R.id.btnBuscar);
        setOnClickListeners(R.id.btnAgregar);
        setOnClickListeners(R.id.btnConfig);
        setOnClickListeners(R.id.btnFacebook);
        setOnClickListeners(R.id.btnInstagram);
        setOnClickListeners(R.id.btnTelefono);
        setOnClickListeners(R.id.btnMensaje);
        setOnClickListeners(R.id.btnGooglemaps);
    }

    private void setOnClickListeners(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                super.onBackPressed();
                break;
            case R.id.btnBuscar:
                intent = new Intent(DetalleContactoActivity.this, BusquedaActivity.class);
                break;
            case R.id.btnFacebook:
                openFacebook();
                break;
            case R.id.btnInstagram:
                openInstagram();
                break;
            case R.id.btnMensaje:
                openMensaje();
                break;
            case R.id.btnTelefono:
                llamar();
                break;
            case R.id.btnGooglemaps:
//                showDireccion();
                break;
            case R.id.btnHome:
                intent = new Intent(DetalleContactoActivity.this, DashboardActivity.class);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdPublicacion(this, "");
                intent = new Intent(DetalleContactoActivity.this, EdicionPublicacionActivity.class);
                startActivity(intent);
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        SessionProcessor.setIdEmpresa(this, "");
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void getEmpresaData() {
        String customUrl = EmpresaService.getURLGetByID(SessionProcessor.getIdEmpresa(this));
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mEmpresa = new Empresa();
                            mEmpresa.setId(obj.getString("id"));
                            mEmpresa.setRazonsocial(obj.getString("razonsocial"));
                            mEmpresa.setCuit(obj.getLong("cuit"));
                            mEmpresa.setMediodepago(obj.getString("mediodepago"));
                            mEmpresa.setEstado(obj.getString("estado"));
                            mEmpresa.setLogo(obj.getString("logo"));
                            mEmpresa.setFacebook(obj.getString("facebook"));
                            mEmpresa.setTwitter(obj.getString("twitter"));
                            mEmpresa.setInstagram(obj.getString("instagram"));
                            mEmpresa.setPalabrasclave(obj.getString("palabrasclave"));
                            mEmpresa.setDirty(obj.getInt("dirty"));
                            mEmpresa.setWeb(obj.getString("web"));
                            mEmpresa.setDescripcion(obj.getString("descripcion"));
                            mEmpresa.setIdcuenta(obj.getString("idcuenta"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
                    add.setMessage(error.getMessage()).setCancelable(true);
                    AlertDialog alert = add.create();
                    alert.setTitle("Sin conexión!!");
                    alert.show();
                }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getImagenes() {
        String customUrl = ImagenService.getURLGetByIDEmpresa(SessionProcessor.getIdEmpresa(this));
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    if(marrGaleria.size() > 0)
                        marrGaleria.clear();

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            Imagen imag = new Imagen();
                            imag.setId(obj.getString("id"));
                            imag.setIdempresa(obj.getString("idempresa"));
                            imag.setUrl(obj.getString("url"));

                            marrGaleria.add(imag);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    madpGaleria.notifyDataSetChanged();
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
            add.setMessage(error.getMessage()).setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Sin conexión!!");
            alert.show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        TextView tv = findViewById(R.id.tvRazonSocial);
        tv.setText(mEmpresa.getRazonsocial());

        tv = findViewById(R.id.tvCuit);
        String value = "CUIT " + mEmpresa.getCuit();
        tv.setText(value);
    }

    private void getSucursales() {
        String customUrl = SucursalService.getURLListByIdEmpresa(SessionProcessor.getIdEmpresa(this));
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        marrSucursales.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Sucursal suc = new Sucursal();
                                suc.setId(obj.getString("id"));
                                suc.setIdempresa(obj.getString("idempresa"));
                                suc.setDireccion(obj.getString("direccion"));
                                suc.setIdpais(obj.getInt("idpais"));
                                suc.setIdprovincia(obj.getInt("idprovincia"));
                                suc.setIdlocalidad(obj.getInt("idlocalidad"));
                                suc.setTelefono(obj.getString("telefono"));
                                suc.setDelivery(obj.getInt("delivery"));
                                suc.setVeinticuatrohs(obj.getInt("veinticuatrohs"));
                                suc.setDiashorarios(obj.getString("diashorarios"));
                                suc.setDirty(obj.getInt("dirty"));
                                suc.setPalabrasclave(obj.getString("palabrasclave"));
                                suc.setLatitud(obj.getDouble("latitud"));
                                suc.setLongitud(obj.getDouble("longitud"));
                                suc.setEstado(obj.getString("estado"));
                                suc.setEstado(obj.getString("razonsocial"));
                                marrSucursales.add(suc);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        madpSucursales.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
                add.setMessage("Información de sucursales no disponible").setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Sucursales");
                alert.show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void openFacebook() {
        String url = mEmpresa.getFacebook();

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setData(Uri.parse(url));
        } catch (Exception e) {
            AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
            add.setMessage("Información no disponible").setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Facebook");
            alert.show();
        }

        startActivity(i);
    }

    private void openInstagram() {
        String url = mEmpresa.getInstagram();

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setData(Uri.parse(url));
        } catch (Exception e) {
            AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
            add.setMessage("Información no disponible").setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Instagram");
            alert.show();
        }

        startActivity(i);
    }

    private void openMensaje() {
        String url = mEmpresa.getTwitter();

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setData(Uri.parse(url));
        } catch (Exception e) {
            AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
            add.setMessage("Información no disponible").setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Twitter");
            alert.show();
        }

        startActivity(i);
    }

    private void llamar() {
        Intent intent = new Intent(Intent.ACTION_DIAL);

        try {
            intent.setData(Uri.parse("tel:" + mEmpresa.getTwitter()));
            startActivity(intent);
        } catch (Exception e) {
            AlertDialog.Builder add = new AlertDialog.Builder(DetalleContactoActivity.this);
            add.setMessage("Información no disponible").setCancelable(true);
            AlertDialog alert = add.create();
            alert.setTitle("Twitter");
            alert.show();
        }
    }

    private class ItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent();
            Sucursal suc = marrSucursales.get(i);
            SessionProcessor.setIdEmpresa(DetalleContactoActivity.this, suc.getIdempresa());
            SessionProcessor.setIdSucursal(DetalleContactoActivity.this, suc.getId());
            intent.setClass(DetalleContactoActivity.this, SucursalVerActivity.class);
            startActivity(intent);
        }
    }
}
