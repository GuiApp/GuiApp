package com.guiapp.guiapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.guiapp.guiapp.classes.DirectionsJSONParser;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.PermissionUtils;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.services.SucursalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SucursalVerActivity extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback {
    public static final String TAG = SucursalVerActivity.class.getSimpleName();

    public static final int LOCATION_UPDATE_MIN_DISTANCE = 1;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;

    Sucursal mSucursal;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;

    private boolean mMapReady;
    private boolean mDataReady;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private double mLatitud;
    private double mLongitud;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if(mLatitud != 0 || mLongitud != 0)
                return;

            if (location != null) {
                Log.d(TAG, String.format("%f, %f", location.getLatitude(), location.getLongitude()));

                if(mMapReady && mDataReady) {
                    mLatitud = location.getLatitude();
                    mLongitud = location.getLongitude();
                    LatLng org = new LatLng(mLatitud, mLongitud);
                    LatLng dest = new LatLng(mSucursal.getLatitud(), mSucursal.getLongitud());
                    getDirectionsUrl(dest, org);
                }

                mLocationManager.removeUpdates(mLocationListener);
            } else {
                Log.d(TAG, "Location is null");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursal_ver);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mMapReady = false;
        mDataReady = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SessionProcessor.getIdSucursal(this).isEmpty()) {
            Utils.showMessage(this, "No se ha seleccionado una sucursal.", true);
        } else {
            getSucursalData();
        }
        setOnClickListeners(R.id.btnVolver);
        setOnClickListeners(R.id.btnHome);
        setOnClickListeners(R.id.btnBuscar);
        setOnClickListeners(R.id.btnAgregar);
        setOnClickListeners(R.id.btnConfig);

        TextView tel = findViewById(R.id.tvTelefonos);
        tel.setOnClickListener(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMapReady = true;
        initMap();
        getCurrentLocation();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (!PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                blankSucursalData();
                SessionProcessor.setActivityPrevia(this, null);
                super.onBackPressed();
                break;
            case R.id.tvTelefonos:
                callTelefono();
                break;
            case R.id.btnHome:
                blankSucursalData();
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                blankSucursalData();
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                blankSucursalData();
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    private void callTelefono() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mSucursal.getTelefono()));
        startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void getCurrentLocation() {
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location location = null;
        mLatitud = SessionProcessor.getLatitud(this);
        mLongitud = SessionProcessor.getLongitud(this);

        if (!(isGPSEnabled || isNetworkEnabled))
            Snackbar.make(mapFragment.getView(), R.string.ubicacion_no_disponible, Snackbar.LENGTH_INDEFINITE).show();
        else {
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);

                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        if (location != null) {
            Log.d(TAG, String.format("getCurrentLocation(%f, %f)", location.getLatitude(),
                    location.getLongitude()));

//            if(mLatitud != 0 || mLongitud != 0){
//                LatLng latlng = new LatLng(mLatitud, mLongitud);
//                drawMarkerLatLng(latlng);
//            } else
//                drawMarker(location);
        }
    }

    private void blankSucursalData() {
        SessionProcessor.setIdSucursal(this, "");
        SessionProcessor.setLatitud(this,0);
        SessionProcessor.setLongitud(this, 0);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        blankSucursalData();
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void getSucursalData() {
        String idsucursal = SessionProcessor.getIdSucursal(this);

        String customUrl = SucursalService.getURLGetByID(idsucursal);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mSucursal = new Sucursal();
                            mSucursal.setId(obj.getString("id"));
                            mSucursal.setDireccion(obj.getString("direccion"));
                            mSucursal.setIdempresa(obj.getString("idempresa"));
                            mSucursal.setTelefono(obj.getString("telefono"));
                            mSucursal.setDelivery(obj.getInt("delivery"));
                            mSucursal.setVeinticuatrohs(obj.getInt("veinticuatrohs"));
                            mSucursal.setDiashorarios(obj.getString("diashorarios"));
                            mSucursal.setLatitud(obj.getDouble("latitud"));
                            mSucursal.setLongitud(obj.getDouble("longitud"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(SucursalVerActivity.this, error.getMessage(), false);
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        SessionProcessor.setIdSucursal(this, mSucursal.getId());
        SessionProcessor.setLatitud(this,mSucursal.getLatitud());
        SessionProcessor.setLongitud(this, mSucursal.getLongitud());

        setTextosTV(R.id.tvSucursal, "Sucursal " + mSucursal.getDireccion());

        setTextosTV(R.id.tvDireccion, mSucursal.getDireccion());
        setTextosTV(R.id.tvTelefonos, mSucursal.getTelefono());
        setTextosTV(R.id.tvHorarios, mSucursal.getDiashorarios());

        setCheckValue(R.id.cbDelivery, mSucursal.getDelivery() == 1);
        setCheckValue(R.id.cb24Hs, mSucursal.getVeinticuatrohs() == 1);

        mDataReady = true;

        if(mMapReady) {
            LatLng org = new LatLng(mLatitud, mLongitud);
            LatLng dest = new LatLng(mSucursal.getLatitud(), mSucursal.getLongitud());
            getDirectionsUrl(dest, org);
        }
    }

    private void setOnClickListeners(int view) {
        ImageButton btn = findViewById(view);
        btn.setOnClickListener(this);
    }

    private void setTextosTV(int id, String text) {
        TextView tv = findViewById(id);
        tv.setText(text);
    }

    private void setCheckValue(int id, boolean value) {
        CheckBox chk = findViewById(id);
        chk.setChecked(value);
        chk.setEnabled(false);
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private void initMap() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result  = googleAPI.isGooglePlayServicesAvailable(this);
        int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        if (result  != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            finish();
        } else {
            if (mMap != null) {
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        }
    }

    private void drawMarker(LatLng org, LatLng dst) {
        if (mMap != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions()
                    .position(org)
                    .title("Estas aquí"));

            mMap.addMarker(new MarkerOptions()
                    .position(dst)
                    .title("Destino"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dst, 12));
        }
    }

    private void getDirectionsUrl(LatLng org, LatLng dest) {
        String str_origin = "origin=" + org.latitude + "," + org.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;
        String output = "json";
        String url = "http://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        drawMarker(org, dest);

        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            if (iStream != null)
                iStream.close();
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return data;
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] url) {
            String data = "";

            try {
                data = downloadUrl(url[0].toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result.toString());

        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points;
            PolylineOptions lineOptions = null;

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.BLUE);
                lineOptions.geodesic(true);

            }
            if(lineOptions != null)
                mMap.addPolyline(lineOptions);
        }
    }
}
