package com.guiapp.guiapp;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.guiapp.guiapp.classes.DatabaseHelper;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Sucursal;
import com.guiapp.guiapp.services.EmpresaService;
import com.guiapp.guiapp.services.FuncionService;
import com.guiapp.guiapp.services.SucursalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SucursalActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int LOCATION_REQUEST = 1;
    Sucursal mSucursal;
    boolean mFromResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursal);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SessionProcessor.getIdSucursal(this).isEmpty()) {
            if(mFromResult)
                updateInfo();
            else
                prepareForNew();
        } else {
            if(!mFromResult) {
                getSucursalData();
            }
        }
        mFromResult = false;

        setOnClickListeners(R.id.btnVolver, false);
        setOnClickListeners(R.id.btnLugar, true);
        setOnClickListeners(R.id.btnGuardar, true);
        setOnClickListeners(R.id.btnCancelar, true);
        setOnClickListeners(R.id.btnHome, false);
        setOnClickListeners(R.id.btnBuscar, false);
        setOnClickListeners(R.id.btnAgregar, false);
        setOnClickListeners(R.id.btnConfig, false);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnVolver:
                blankSucursalData();
                SessionProcessor.setActivityPrevia(this, null);
                super.onBackPressed();
                break;
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnCancelar:
                blankSucursalData();
                super.onBackPressed();
                break;
            case R.id.btnLugar:
                prepareToLocation();
                break;
            case R.id.btnHome:
                blankSucursalData();
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                blankSucursalData();
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                blankSucursalData();
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(view);
                openContextMenu(view);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        SessionProcessor.setActivityPrevia(this, null);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFromResult= true;
        if (resultCode == RESULT_OK) {
            if(mSucursal != null) {
                mSucursal.setLatitud(SessionProcessor.getLatitud(SucursalActivity.this));
                mSucursal.setLongitud(SessionProcessor.getLongitud(SucursalActivity.this));
            }
        }
    }

    private void prepareToLocation() {
        mSucursal.setDireccion(getTextFromTV(R.id.etDireccion));
        mSucursal.setIdempresa(SessionProcessor.getIdEmpresa(this));
        mSucursal.setTelefono(getTextFromTV(R.id.etTelefonos));
        mSucursal.setDelivery(getValueFromCB(R.id.cbDelivery));
        mSucursal.setVeinticuatrohs(getValueFromCB(R.id.cb24Hs));
        mSucursal.setDiashorarios(getTextFromTV(R.id.etHorarios));
        MainApp.setSucursal(mSucursal);
        Intent intent = new Intent(this, DireccionActivity.class);
        startActivityForResult(intent, LOCATION_REQUEST);

        //SessionProcessor.setActivityPrevia(this, SucursalActivity.class.getSimpleName());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void blankSucursalData() {
        SessionProcessor.setIdSucursal(this, "");
        SessionProcessor.setLatitud(this,0);
        SessionProcessor.setLongitud(this, 0);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }
        blankSucursalData();
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void prepareForNew() {
        mSucursal = new Sucursal();
        blankSucursalData();

        setTextosTV(R.id.tvSucursal, "Detalle de Sucursal");

        setTextos(R.id.etDireccion, "");
        setTextos(R.id.etTelefonos, "");
        setTextos(R.id.etHorarios, "");

        setCheckValue(R.id.cbDelivery, false);
        setCheckValue(R.id.cb24Hs, false);
    }

    private void setTextos(int id, String text) {
        EditText et = findViewById(id);
        et.setText(text);
    }

    private void setTextosTV(int id, String text) {
        TextView tv = findViewById(id);
        tv.setText(text);
    }

    private void setCheckValue(int id, boolean value) {
        CheckBox chk = findViewById(id);
        chk.setChecked(value);
    }

    private void getSucursalData() {
        String idsucursal = SessionProcessor.getIdSucursal(this);

        String customUrl = SucursalService.getURLGetByID(idsucursal);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mSucursal = new Sucursal();
                            mSucursal.setId(obj.getString("id"));
                            mSucursal.setDireccion(obj.getString("direccion"));
                            mSucursal.setIdempresa(obj.getString("idempresa"));
                            mSucursal.setTelefono(obj.getString("telefono"));
                            mSucursal.setDelivery(obj.getInt("delivery"));
                            mSucursal.setVeinticuatrohs(obj.getInt("veinticuatrohs"));
                            mSucursal.setDiashorarios(obj.getString("diashorarios"));
                            mSucursal.setLatitud(obj.getDouble("latitud"));
                            mSucursal.setLongitud(obj.getDouble("longitud"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(SucursalActivity.this, error.getMessage(), false);
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        SessionProcessor.setIdSucursal(this, mSucursal.getId());
        SessionProcessor.setLatitud(this,mSucursal.getLatitud());
        SessionProcessor.setLongitud(this, mSucursal.getLongitud());

        setTextosTV(R.id.tvSucursal, "Sucursal " + mSucursal.getDireccion());

        setTextos(R.id.etDireccion, mSucursal.getDireccion());
        setTextos(R.id.etTelefonos, mSucursal.getTelefono());
        setTextos(R.id.etHorarios, mSucursal.getDiashorarios());

        setCheckValue(R.id.cbDelivery, mSucursal.getDelivery() == 1);
        setCheckValue(R.id.cb24Hs, mSucursal.getVeinticuatrohs() == 1);
    }

    private void setOnClickListeners(int view, boolean isButton) {
        if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }

    private void guardar() {
        if (mSucursal.getId().isEmpty())
            post();
        else
            put();
    }

    private void post() {
        mSucursal.setDireccion(getTextFromTV(R.id.etDireccion));
        mSucursal.setIdempresa(SessionProcessor.getIdEmpresa(this));
        mSucursal.setTelefono(getTextFromTV(R.id.etTelefonos));
        mSucursal.setDelivery(getValueFromCB(R.id.cbDelivery));
        mSucursal.setVeinticuatrohs(getValueFromCB(R.id.cb24Hs));
        mSucursal.setDiashorarios(getTextFromTV(R.id.etHorarios));

        if (mSucursal.getDireccion().isEmpty()) {
            Utils.showMessage(SucursalActivity.this, "Faltan completar datos!!", false);
            return;
        }

        if(mSucursal.getLatitud() == 0  && mSucursal.getLongitud() == 0) {
            Utils.showMessage(this, "No ha ingresado coordenadas!!\n Su comercio o servicio no será encontrado!!\nPor favor ingrese coordenadas.", false);
            return;
        }
        mSucursal.setId(UUID.randomUUID().toString());

        HashMap<String, String> map = mSucursal.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = SucursalService.API_CALL;

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.POST,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.showMessage(SucursalActivity.this, "La sucursal se ha guardado con éxito!!", false);
                        MainApp.setSucursal(null);
                        if (Utils.DEBUG)
                            Toast.makeText(SucursalActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                        getFunciones();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(SucursalActivity.this, "La sucursal no se ha podido guardar!! \n Por favor reintente.", false);
                        mSucursal.setId("");
                        if (Utils.DEBUG)
                            Toast.makeText(SucursalActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void getFunciones() {
        DatabaseHelper dBHelper = DatabaseHelper.getInstance(this);
        SQLiteDatabase mDatabase = dBHelper.getReadableDatabase();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(mDatabase);

        if(cuenta.getSync() == 1)
            return;

        String url = FuncionService.getURLGetByIDCuenta(cuenta.getId());
        JsonArrayRequest jsonreq = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        SessionProcessor.setFunciones(SucursalActivity.this, response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Utils.DEBUG)
                    Toast.makeText(SucursalActivity.this, "Error: getFunciones",Toast.LENGTH_SHORT).show();
            }
        });
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void put() {
        String idempresa = SessionProcessor.getIdEmpresa(this);
        mSucursal.setIdempresa(idempresa);
        mSucursal.setDireccion(getTextFromTV(R.id.etDireccion));
        mSucursal.setIdempresa(SessionProcessor.getIdEmpresa(this));
        mSucursal.setTelefono(getTextFromTV(R.id.etTelefonos));
        mSucursal.setDelivery(getValueFromCB(R.id.cbDelivery));
        mSucursal.setVeinticuatrohs(getValueFromCB(R.id.cb24Hs));
        mSucursal.setDiashorarios(getTextFromTV(R.id.etHorarios));
        mSucursal.setDirty(0);

        if(mSucursal.getDireccion().isEmpty()) {
            Utils.showMessage(this, "Faltan completar datos!!", false);
            return;
        }

        if(mSucursal.getLatitud() == 0  && mSucursal.getLongitud() == 0) {
            Utils.showMessage(this, "No ha ingresado coordenadas!!\n Su comercio o servicio no será encontrado!!\nPor favor ingrese coordenadas.", false);
            return;
        }

        HashMap<String, String> map = mSucursal.getHashMap();
        JSONObject jobject = new JSONObject(map);
        String customUrl = SucursalService.getURLPut(mSucursal.getId());

        JsonObjectRequest jsonreq = new JsonObjectRequest(
                Request.Method.PUT,
                customUrl,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.showMessage(SucursalActivity.this, "La sucursal se ha guardado con éxito!!", false);
                        if (Utils.DEBUG)
                            Toast.makeText(SucursalActivity.this, "Grabado con éxito", Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(SucursalActivity.this, "La sucursal no se ha podido guardar!! \n Por favor reintente.", false);
                        if (Utils.DEBUG)
                            Toast.makeText(SucursalActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private String getTextFromTV(int id) {
        EditText edittext = findViewById(id);
        return edittext.getText().toString();
    }

    private int getValueFromCB(int id) {
        CheckBox checkBox = findViewById(id);
        return checkBox.isChecked() ? 1 : 0;
    }
}
