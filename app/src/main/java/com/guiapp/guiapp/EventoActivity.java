package com.guiapp.guiapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.guiapp.guiapp.adapters.EventoAdapter;
import com.guiapp.guiapp.classes.MainApp;
import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;
import com.guiapp.guiapp.entities.Cuenta;
import com.guiapp.guiapp.entities.Evento;
import com.guiapp.guiapp.entities.Paquete;
import com.guiapp.guiapp.services.EventoService;
import com.guiapp.guiapp.services.PaqueteService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EventoActivity extends AppCompatActivity implements View.OnClickListener {
    private Evento mEvento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evento);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
    }

    @Override
    protected  void onResume() {
        super.onResume();

        if (SessionProcessor.getIdEvento(this) == "") {
            Utils.showMessage(this, "No se ha seleccionado un evento.", true);
        } else {
            getEventoData();
        }
        setOnClickListeners(R.id.btnVolver, false);
        setOnClickListeners(R.id.btnEditar, true);
        setOnClickListeners(R.id.btnHome, false);
        setOnClickListeners(R.id.btnBuscar, false);
        setOnClickListeners(R.id.btnAgregar, false);
        setOnClickListeners(R.id.btnConfig, false);
    }

    private void getEventoData() {
        String idEvento = SessionProcessor.getIdEvento(this);

        String customUrl = EventoService.getURLGetByID(idEvento);
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            mEvento = new Evento();
                            mEvento.setId(obj.getString("id"));
                            mEvento.setNombre(obj.getString("nombre"));
                            mEvento.setDescripcion(obj.getString("descripcion"));
                            mEvento.setImagen(obj.getString("imagen"));
                            mEvento.setIdtipo(obj.getLong("idtipo"));
                            mEvento.setTipo(obj.getString("tipo"));
                            mEvento.setUbicacion(obj.getString("ubicacion"));
                            mEvento.setFecinicio(obj.getString("fecinicio"));
                            mEvento.setFecfin(obj.getString("fecfin"));
                            mEvento.setIdempresa(obj.getString("idempresa"));
                            mEvento.setRazonsocial(obj.getString("razonsocial"));
                            mEvento.setHorainicio(obj.getString("horainicio"));
                            mEvento.setHorafin(obj.getString("horafin"));
                            mEvento.setIdcuenta(obj.getString("idcuenta"));

                            updateInfo();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(EventoActivity.this, error.getMessage(), false);
                    }}
        );
        MainApp.getPermission().addToRequestQueue(jsonreq);
    }

    private void updateInfo() {
        setTextosTV(R.id.tvTipoEvento, mEvento.getTipo());
        setTextosTV(R.id.tvTitulo, mEvento.getNombre());
        setTextosTV(R.id.tvFecha, mEvento.getFormatedFecinicio());
        setTextosTV(R.id.tvLugar, "Lugar | " + mEvento.getUbicacion());
        String[] hora =  mEvento.getHorainicio().split(":");
        setTextosTV(R.id.tvHorario, "Horario | " + hora[0] + ":" + hora[1]);
        setTextosTV(R.id.tvDescripcion, mEvento.getDescripcion());

        ImageView imageView = findViewById(R.id.ivImagen);
        try {
            Picasso.get().load(mEvento.getImagen()).fit().into(imageView);
        } catch (Exception e) {}

        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(!idcuenta.isEmpty()) {
            if(idcuenta.equals(mEvento.getIdcuenta())) {
                showEditar();
            }
        }
    }

    private void showEditar() {
        Button btn = findViewById(R.id.btnEditar);
        btn.setVisibility(View.VISIBLE);
    }

    private void setTextosTV(int id, String text) {
        TextView tv = findViewById(id);
        tv.setText(text);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnVolver:
                onBackPressed();
                break;
            case R.id.btnEditar:
                intent = new Intent(EventoActivity.this, EventoABMActivity.class);
                break;
            case R.id.btnHome:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
                break;
            case R.id.btnBuscar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, BusquedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btnAgregar:
                SessionProcessor.setIdEvento(this, "-1");
                intent = new Intent(this, EdicionPublicacionActivity.class);
                startActivity(intent);
                break;
            case R.id.btnConfig:
                registerForContextMenu(v);
                openContextMenu(v);
                break;
        }

        if(intent != null)
            startActivity(intent);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.general_config, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_cuenta:
                goToCuenta();
                return true;
            case R.id.action_empresas:
                goToEmpresas();
                return true;
            case R.id.action_terminos:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void goToCuenta() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.equals(Cuenta.INVITADO)) {
            Utils.showMessage(this, "Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, CuentaActivity.class);
        startActivity(intent);
    }

    private void goToEmpresas() {
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas.", false);
            return;
        }
        Intent intent = new Intent(this, EmpresasActivity.class);
        startActivity(intent);
    }

    private void setOnClickListeners(int view, boolean isButton) {
        if(isButton) {
            Button btn = findViewById(view);
            btn.setOnClickListener(this);
        } else {
            ImageButton btn = findViewById(view);
            btn.setOnClickListener(this);
        }
    }
}
