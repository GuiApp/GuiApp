package com.guiapp.guiapp;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.guiapp.guiapp.classes.SessionProcessor;
import com.guiapp.guiapp.classes.Utils;

public class PagoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pago);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        int idPaquete = SessionProcessor.getIdPaquete(this);
        if (idPaquete == -1) {
            Utils.showMessage(this, "No se ha seleccionado un paquete.", true);
            return;
        }
        String idcuenta = SessionProcessor.getIdCuenta(this);
        if(idcuenta.isEmpty()) {
            Utils.showMessage(this,"Necesita registrarse para poder dar de alta empresas..", false);
            return;
        }

        WebView webView = findViewById(R.id.wvPago);
/*      WebSettings webSettings = webView.getSettings();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUserAgentString("Android");*/
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
/*        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);*/
        webView.setWebChromeClient(new WebChromeClient());
/*        webView.setWebViewClient(new WebViewClient());*/
        if(Utils.DEBUG) {
            webView.loadUrl("https://192.168.1.24/guiappmp/compra/index.php?" +
                    "idpaq="+ idPaquete+ "&extref="+ idcuenta);
        } else {
            webView.loadUrl("https://www.guiappworldwide.com/guiappmp/compra/index.php?" +
                    "idpaq=" + idPaquete + "&extref=" + idcuenta);
        }
//        getPaqueteData();
//        webView.loadUrl("http://www.guiappworldwide.com/guiappmp/compra/index.php?" +
//                "idpaq="+ idPaquete+ "&extref="+ idcuenta);
    }

//    private void getPaqueteData() {
//        int idPaquete = SessionProcessor.getIdPaquete(this);
//
//        String customUrl = PaqueteService.getURLGetByID(idPaquete);
//        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        try {
//                            JSONObject obj = response.getJSONObject(0);
//                            Paquete mPaquete = new Paquete();
//                            mPaquete.setId(obj.getInt("id"));
//                            mPaquete.setImagen(obj.getString("imagen"));
//                            mPaquete.setNombre(obj.getString("nombre"));
//                            mPaquete.setDescripcion(obj.getString("descripcion"));
//                            mPaquete.setCosto(obj.getDouble("costo"));
//                            mPaquete.setDuracion(obj.getInt("duracion"));
//                            mPaquete.setLogo(obj.getString("logo"));
//                            mPaquete.setPlazo(obj.getString("plazo"));
//
//                            updateInfo(mPaquete);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Utils.showMessage(PagoActivity.this, error.getMessage(), false);
//                    }}
//        );
//        MainApp.getPermission().addToRequestQueue(jsonreq);
//    }

//    private void updateInfo(Paquete paquete) {
//
//        BigDecimal costo = new BigDecimal(paquete.getCosto());
//        CheckoutPreference checkoutPreference = new CheckoutPreference.Builder()
//                .addItem(new Item("Item", costo)))
//                .setSite(Sites.ARGENTINA)
//                .setMaxInstallments(1) //Limit the amount of installments
//                .build();
//        startMercadoPagoCheckout(checkoutPreference);
//
//    }
}
